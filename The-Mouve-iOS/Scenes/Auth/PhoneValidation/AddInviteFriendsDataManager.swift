//
//  AddInviteFriendsViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/19/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import DigitsKit

class AddInviteFriendsDataManager: NSObject {
    var contacts: [ContactViewModel] {
        return fetchFriends()
    }
    var digitFriends: [DGTUser] = [DGTUser]()
//    var nonAppFriends: 
    func findDigitsFriends(session: DGTSession) {
        let digitsContacts = DGTContacts(userSession: session)
        // looking up friends happens in batches. Pass nil as cursor to get the first batch.
        digitsContacts?.lookupContactMatches(withCursor: nil) { (matches, nextCursor, error) -> Void in
            // If nextCursor is not nil, you can continue to call lookupContactMatchesWithCursor: to retrieve even more friends.
            // Matches contain instances of DGTUser. Use DGTUser's userId to lookup users in your own database.
            log.info("Friends:")
            guard let friends = matches as? [DGTUser] else{
                log.info("Couldn't convert matches from Digits to friends")
                return
            }
            self.digitFriends = friends
        }
    }
    func fetchFriends() -> [ContactViewModel]{
        return dataManager.fakeData.users.map{ContactViewModel(user: $0)}
    }
}
