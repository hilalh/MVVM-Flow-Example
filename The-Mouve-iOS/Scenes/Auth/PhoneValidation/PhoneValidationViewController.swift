//
//  PhoneValidationViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/18/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import DigitsKit
import EZSwipeController

class PhoneValidationViewController: UIViewController, DGTAuthEventDelegate {
    let digits = Digits.sharedInstance()
//    let session = DGTDebugConfiguration.defaultDebugSession()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        digits.logOut()
//        digits.debugOverrides = DGTDebugConfiguration(successStateWithDigitsSession: session)
        self.view.backgroundColor = UIColor.white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.digits.authEventDelegate = self
        let authConf = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
        authConf?.appearance = DGTAppearance.setDigitsAppearance(mainColor: kThemeMainColor)
        authConf?.title = "Verify Phone"
        self.digits.authenticate(with: self, configuration: authConf!){ session in
            let vc = AddInviteFriendsViewController()
            let nav = UINavigationController(rootViewController: vc)
            self.present(nav, animated: true){
             vc.onboardForm()
            }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func digitsAuthenticationDidComplete(_ authEventDetails: DGTAuthEventDetails!) {
        guard let phoneNumber = (digits.session()?.phoneNumber) else{
            return
        }
        dataManager.userOps.setDetails(userField: "phoneNumber", fieldValue: phoneNumber){(success, error) in
            guard let error = error else {
                return
            }
            //Print error and have user retry the process
            log.info(error)
            self.digits.authEventDelegate?.digitsUserRetry!(onErrorRescueScreen: authEventDetails)
        }
    }
    

}

