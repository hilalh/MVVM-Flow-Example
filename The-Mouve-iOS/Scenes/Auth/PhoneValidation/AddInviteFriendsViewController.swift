//
//  AddPhoneContactsViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/18/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import DigitsKit

class AddInviteFriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DGTCompletionViewController, UISearchBarDelegate {
    //UISearchBarDelegate, UISearchDisplayDelegate, UISearchResultsUpdating
    var tableView: UITableView!
    //var searchController = UISearchController(searchResultsController: nil)
    lazy var searchBar:UISearchBar = UISearchBar()

    var viewModel: AddInviteFriendsDataManager? = AddInviteFriendsDataManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setTableView()
        setSearchBar()
        // Do any additional setup after loading the view.
    }
    
    
    func setSearchBar(){
        /*
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar

        //toTextField.inputView = self.tableView
        searchController.delegate = self
        searchController.dimsBackgroundDuringPresentation = false // default is YES
        searchController.searchBar.delegate = self    // so we can monitor text changes + others

        /*
         Search is now just presenting a view controller. As such, normal view controller
         presentation semantics apply. Namely that presentation will walk up the view controller
         hierarchy until it finds the root view controller or one that defines a presentation context.
         */

        definesPresentationContext = true
         */

        searchBar.searchBarStyle = UISearchBarStyle.prominent
        searchBar.placeholder = " Search..."
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        let searchTextField = searchBar.value(forKey: "searchField") as? UITextField
        searchTextField?.layer.borderColor = kThemeMainColor.cgColor
        searchTextField?.layer.borderWidth = 0.5
        searchTextField?.layer.cornerRadius = 10
        tableView.tableHeaderView = searchBar
    }

    func setTableView(){
        tableView = UITableView(frame: UIScreen.main.bounds, style: UITableViewStyle.plain)
        tableView?.delegate = self
        tableView?.dataSource = self
        //        tableView?.tableHeaderView = TableHeader()
        self.view.addSubview(tableView!)
        tableView?.register(ContactsTableViewCell.self, forCellReuseIdentifier: "DefaultCell")
    }



    func digitsAuthenticationFinished(with session: DGTSession!, error: Error!) {
        let digitsContacts = DGTContacts(userSession: session)
        digitsContacts?.startUpload(withDigitsAppearance: DGTAppearance.setDigitsAppearance(mainColor: kThemeMainColor), presenterViewController: self, title: "Verify Phone") { result, error in
            if result != nil {
                // The result object tells you how many of the contacts were uploaded.
                log.info("Total contacts: \(result?.totalContacts), uploaded successfully: \(result?.numberOfUploadedContacts)")
            }
            self.viewModel?.findDigitsFriends(session: session)
        }
    }

    func onboardForm(){
        let button: UIBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(presentHome))
        button.tintColor = UIColor.white
        setNavBar(title: "Add Friends", leftButton: nil, rightButton: button)
    }
    
    @objc private func presentHome(){
        UIApplication.shared.keyWindow?.rootViewController = UIApplication.shared.MainVC()
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        setNavBar(title: "Add Friends")

    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return (viewModel?.contacts.count)!
        default:
            return 0
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0:
            var cell: UITableViewCell?
            if(indexPath.row == 0){
                cell = UITableViewCell.arrowedCell(title: "Invite from Facebook",mainColor: kThemeMainColor)
            }
            else{
                cell = UITableViewCell.arrowedCell(title: "Invite from Contacts",mainColor: kThemeMainColor)
            }
            cell?.selectionStyle = .none
            return cell!
        default:
            let person = viewModel?.contacts[indexPath.row]
            var cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath as IndexPath) as? ContactsTableViewCell
            cell?.addCell(viewModel: person!)
            if cell == nil {
                 cell = ContactsTableViewCell.addCell(viewModel: person!)
            }
            return cell!
        }

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UITableViewController()
        switch indexPath.section{
            case 0:
                //self.present(vc, animated: true, completion: nil)
                self.show(vc, sender: self)
            case 1:
                self.present(vc, animated: true, completion: nil)
            default:
                tableView.deselectRow(at: indexPath, animated: true)
        }
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 2

    }




    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
