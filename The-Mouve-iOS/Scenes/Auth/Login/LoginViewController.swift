//
//  LoginViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/25/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Firebase
import FacebookLogin
import FacebookCore

import ChameleonFramework

class LoginViewController: UIViewController {
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let user = FIRAuth.auth()?.currentUser else {
            setupButton()
            return
        }
        dataManager.userOps.signedIn(user){
            self.passToNextVC()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white



        
        

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /// Handles login button from facebook
    func setupButton(){
        
        let btnSize : CGFloat = 60
        let btnLogin = UIButton(frame: CGRect(x:0,y:0,width:btnSize*5,height:btnSize))
        btnLogin.center = CGPoint(x: view.center.x, y: (view.center.y * 1.75) )
        btnLogin.setTitle(" Connect with Facebook", for: .normal)
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnLogin.backgroundColor = kThemeBrighterColor
        btnLogin.setImage(#imageLiteral(resourceName: "ic_facebook.png").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        btnLogin.imageView?.tintColor = UIColor.white
        btnLogin.addTarget(self, action: #selector(loginButtonPressed), for: UIControlEvents.touchUpInside)
        
        //Circular button
        btnLogin.layer.cornerRadius = 5
        btnLogin.layer.masksToBounds = true
        
        self.view.addSubview(btnLogin)
    }
    
    func loginButtonPressed() {
        dataManager.userOps.authenticateUser(){ (user, error) in
            if let error = error {
                log.error(error.localizedDescription)
                ErrorHandler.showMessage(text: error.localizedDescription)
                return
            }
            else {
                dataManager.userOps.signedIn(user){
                    self.passToNextVC()
                }
            }
        }
    }
    /// Pass To Sign Up Page
    func passToSignUpVC() {
//        let vc = PhoneValidationViewController()
        
        let vc = SignUpViewController()
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    func passToPhoneValidation(){
        let vc = PhoneValidationViewController()
        let nav = UINavigationController(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    /// Pass To Main Screen Page
    func passToMainScreenVC() {
        
//        self.show(UIApplication.shared.MainVC(), sender: self)
        present(UIApplication.shared.MainVC(), animated: true, completion: nil)
    }
    /// Uses both functions above to go to next VC based on whether user is new or no.
    func passToNextVC() {
        dataManager.userOps.isUserFieldExisting(fieldName: kFirebaseUsername) { result in
            if result == false {
                self.passToSignUpVC()
            } else {
                dataManager.userOps.isUserFieldExisting(fieldName: kFirebasePhone) { result in
                    if result == false {
                        self.passToPhoneValidation()
                    } else {

                        self.passToMainScreenVC()
                    }
                }
            }
        }
    }
}
