//
//  SignUpViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/17/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Firebase


class SignUpViewModel {

    let disposeBag = DisposeBag()
    var username: BehaviorSubject<String>
    var profile: BehaviorSubject<FacebookProfile?>


    init(){
        var user = FUser(userData: (FIRAuth.auth()?.currentUser)!)
            // User is signed in.
        self.profile = BehaviorSubject<FacebookProfile?>(value: nil)
        self.username = BehaviorSubject<String>(value: user.username)
        
        self.username.subscribe(onNext: { (username) in
            user.username = username
        }).addDisposableTo(disposeBag)
        self.fetchFacebook()
    }
    
    private func fetchFacebook(){
        dataManager.facebook.fetchDetails { (profile, error) in
            guard let error = error else{
                guard let profile = profile else{
                    log.error("Profile is empty!")
                    return
                }
                self.profile = BehaviorSubject<FacebookProfile?>(value: profile)
                return
            }
            log.error("\(error)")
        }
    }
}



