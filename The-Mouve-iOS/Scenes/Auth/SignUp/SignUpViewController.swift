
///
//  SignUpViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Eureka
import RxSwift
import RxCocoa
import SwiftMessages



class SignUpViewController: FormViewController {


    fileprivate let viewModel: SignUpViewModel = SignUpViewModel()
    fileprivate let disposeBag = DisposeBag()
    
    var nameRow: TextRow = TextRow()
    var usernameRow: AccountRow = AccountRow()
    var genderRow: TextRow = TextRow()
    var bdayRow: DateRow = DateRow()
    var emailRow: EmailRow = EmailRow()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "cross-cancel-icon"), style: .plain, target: self, action: #selector(cancelProcess))
        let rightButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(setUsername))
        setNavBar(title: "Sign Up", leftButton: leftButton, rightButton: rightButton)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateFields()
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        form = Section("Fill in")
            <<< AccountRow(){ row in
                row.title = "Username"
                row.tag = "username"
                row.placeholder = "Enter text here"
            }
            +++ Section("Confirm these")
            <<< TextRow(){ row in
                row.title = "Full name"
                row.tag = "name"
                row.disabled = true
            }
            <<< TextRow(){ row in
                row.title = "Gender"
                row.tag = row.title?.lowercased()
                row.disabled = true
            }
            <<< EmailRow(){ row in
                row.title = "Email"
                row.tag = row.title?.lowercased()
                row.disabled = true
            }
            <<< DateRow(){ row in
                row.title = "Birthday"
                row.tag = "birthday"
                row.disabled = true
            }
            <<< ButtonRow(){ row in
                row.title = "Save"
                row.onCellSelection{(cell,row) in
                    self.setUsername()
                }
            }
        
        nameRow = form.rowBy(tag: "name")!
        genderRow = form.rowBy(tag: "gender")!
        bdayRow = form.rowBy(tag: "birthday")!
        emailRow = form.rowBy(tag: "email")!
        usernameRow = form.rowBy(tag: "username")!        
    }
    
    func updateFields(){
        viewModel.profile.subscribe(onNext: {result in
            self.nameRow.value = result?.name
            self.genderRow.value = result?.gender
            self.bdayRow.value = result?.birthday
            self.emailRow.value = result?.email
            for row in self.form.allRows{
                row.updateCell()
            }
        }).addDisposableTo(disposeBag)
    }
    
    @objc private func cancelProcess(){
        dataManager.userOps.logOut()
    }
    
    @objc private func setUsername(){
        dataManager.userOps.isUsernameAvailable(username: self.usernameRow.value!){ success in
            guard success else {
                ErrorHandler.showMessage(text: kErrorUsernameAlreadyTaken)
                return
            }
            dataManager.userOps.setUsername(username: self.usernameRow.value!){ success, error in
                guard success else {
                    ErrorHandler.showMessage(text: kErrorSetUsername)
                    return
                }
                self.navigationController?.pushViewController(PhoneValidationViewController(), animated: true)
            }
        }
    }


}


