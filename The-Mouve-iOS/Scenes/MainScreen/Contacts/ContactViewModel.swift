//
//  ContactViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/11/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ContactViewModel: NSObject {
    let disposeBag = DisposeBag()
    private var user: abstractUser
    
    var fullName: BehaviorSubject<String>{
        return BehaviorSubject<String>(value: user.fullName)
    }
    var username: BehaviorSubject<String>{
        return BehaviorSubject<String>(value: user.username)
    }
    var picURL: BehaviorSubject<URL>{
        return BehaviorSubject<URL>(value: URL(string: user.profileImageURL!)!)
    }
    var active: BehaviorSubject<Bool>{
        return BehaviorSubject<Bool>(value: fetchActive())
    }
    
    
    init(user: abstractUser){
        self.user = user
        super.init()
        /*
        fullName.subscribe(onNext: { text in
            
        }, onError: { error in
            
        }, onCompletiond: nil, onDisposed: nil)
        
        username.subscribe(onNext: { text in
            
        }, onError: { error in
            
        }, onCompletiond: nil, onDisposed: nil)
        
        picURL.subscribe(onNext: { url in
            
        }, onError: { error in
            
        }, onCompletiond: nil, onDisposed: nil)
        
        active.subscribe(onNext: { text in
            
        }, onError: { error in
            
        }, onCompletiond: nil, onDisposed: nil)
        */
    }
    
    
    func fetchActive() -> Bool{
        return arc4random_uniform(2) == 0
    }
    /*
    func fetchDetails(){
        //let user = FUser.demo(username: "hmoneygotit", fullName: "Hilal Habashi", picURL: "https://scontent-ort2-1.xx.fbcdn.net/t31.0-8/14435128_10154570343774324_3621945643097376691_o.jpg")
        self.fullName = user.fullName
        self.username = user.username
        self.picURL = user.profileImageURL!
    }
    */
}
