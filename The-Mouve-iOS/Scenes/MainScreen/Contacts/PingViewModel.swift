//
//  PingViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/8/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PingViewModel: NSObject {
    private var ping: abstractPing
    private var user: BehaviorSubject<abstractUser>{
        return BehaviorSubject<abstractUser>(value: fetchCreator())
    }
    
    let disposeBag = DisposeBag()

    var fullName: BehaviorSubject<String>{
        return try! BehaviorSubject<String>(value: user.value().fullName)
    }
    var username: BehaviorSubject<String>{
        return try! BehaviorSubject<String>(value: user.value().username)
    }
    var picURL: BehaviorSubject<URL>{
        return try! BehaviorSubject<URL>(value: URL(string: user.value().profileImageURL!)!)
    }
    var active: BehaviorSubject<Bool>{
        return BehaviorSubject<Bool>(value: fetchActive())
    }
    var time: BehaviorSubject<String>{
        return BehaviorSubject<String>(value: ping.timeStamp)
    }
    
    init(ping: abstractPing){
        self.ping = ping
    }
    
    func fetchCreator() -> FUser{
       return ping.creator
    }
    
    func fetchActive() -> Bool{
        return arc4random_uniform(2) == 0
    }
    
    func markRead(){
        
    }
    
    func recordLocationOfReceiver(){
        
    }
    
    func recordLocationOfSender(){
        
    }
    
    func calculateTimeSincePing(){
        
    }
}
