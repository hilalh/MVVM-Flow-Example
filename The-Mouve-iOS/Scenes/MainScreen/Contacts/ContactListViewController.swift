//
//  ContactTableViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/6/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Foundation
import GSKStretchyHeaderView

class ContactListViewController: UIViewController {
    var addPeople: UIBarButtonItem!
    var selectedContact: ContactViewModel?
    var settingsBtn: UIBarButtonItem!
    var tableView: UITableView?
    var viewModel: ContactListDataManager?


    override func viewDidLoad() {
        super.viewDidLoad()
        //Setting demo user
        self.view.backgroundColor = UIColor.white
        viewModel = ContactListDataManager(user: AppState.sharedInstance.myUser!)
        self.configureTableView()
        let stretchyHeader = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200))
        stretchyHeader.stretchDelegate = self
        let header = ContactListHeaderView(viewModel: (viewModel?.userViewModel)!)
//        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
        header.frame = stretchyHeader.frame
        header.delegate = self
        header.setButtons(rightImage: #imageLiteral(resourceName: "settings-icon"), leftImage: #imageLiteral(resourceName: "addperson"))
        stretchyHeader.addSubview(header)
        
        
//        self.tableView?.tableHeaderView = stretchyHeader
        self.tableView?.addSubview(stretchyHeader)

    }
}
extension ContactListViewController: GSKStretchyHeaderViewStretchDelegate{
    func stretchyHeaderView(_ headerView: GSKStretchyHeaderView, didChangeStretchFactor stretchFactor: CGFloat) {
        headerView.expansionMode = .topOnly
        
        // You can change the minimum and maximum content heights
        headerView.minimumContentHeight = UIApplication.shared.statusBarFrame.height // you can replace the navigation bar with a stretchy header view
        headerView.maximumContentHeight = 220
        
        // You can specify if the content expands when the table view bounces, and if it shrinks if contentView.height < maximumContentHeight. This is specially convenient if you use auto layout inside the stretchy header view
        headerView.contentShrinks = headerView.contentView.frame.height < headerView.maximumContentHeight
        headerView.contentExpands = true // useful if you want to display the refreshControl below the header view
        
        
        // You can specify wether the content view sticks to the top or the bottom of the header view if one of the previous properties is set to NO
        // In this case, when the user bounces the scrollView, the content will keep its height and will stick to the bottom of the header view
        headerView.contentAnchor = .top
    }
}

extension ContactListViewController: UITableViewDelegate, UITableViewDataSource{
    

    func configureTableView(){
        self.tableView = UITableView(frame: self.view.frame)
        self.tableView?.backgroundColor = UIColor.clear
        self.tableView?.center.x = self.view.center.x
        self.tableView?.showsVerticalScrollIndicator = false
        self.tableView?.delegate = self
        self.tableView?.dataSource = self
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView?.estimatedRowHeight = 40
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.register(ContactsTableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        self.tableView?.register(ScrollablePeopleTableViewCell.self, forCellReuseIdentifier: "PingsCell")
        self.view.addSubview(self.tableView!)

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            guard let tableViewCell = cell as? ScrollablePeopleTableViewCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.section, cellIdentifier: "PingCell")

        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if(indexPath.section == 0){

            let cell: ScrollablePeopleTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "PingsCell", for: indexPath) as? ScrollablePeopleTableViewCell
            cell?.setCollectionView()
            cell?.setPingsColl()
            //Resizing width

            return cell!
        }else{
            let contact = viewModel?.friends[indexPath.section - 1]
            var cell: ContactsTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath as IndexPath) as? ContactsTableViewCell
            cell?.setupView(viewModel: contact!)
            cell?.clipsToBounds = true
            if cell == nil {
                cell = ContactsTableViewCell(viewModel: contact!)
            }
            return cell!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedContact = viewModel?.friends[indexPath.section - 1]
        showProfile(user: selectedContact!)
    }
    func showProfile(user: ContactViewModel){
        let vc  = ProfilePopupView(_viewModel: user)
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return (viewModel?.friends.count)! + 1
    }
}

extension ContactListViewController: ContactListHeaderViewDelegate{
    func addPersonTapped(){
        log.info("Add person tapped")

        self.present(UINavigationController(rootViewController: ManageFriendsViewController()), animated: true, completion: nil)
    }

    func settingsTapped(){
        self.present(UINavigationController(rootViewController: SettingsViewController()), animated: true, completion: nil)
        log.info("Settings tapped")
    }

    func leftButtonTapped() {
        addPersonTapped()
    }

    func rightButtonTapped() {
        settingsTapped()
    }
}
extension ContactListViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        let pingsCount = viewModel?.pings.count
        return (pingsCount!)
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PingCell",
                                                      for: indexPath) as? PeopleCollectionViewCell
        let ping = viewModel?.pings[indexPath.item]
        cell?.setCell(ping: ping!)

        return cell!
    }

    func showPing(ping: PingViewModel){
        let vc = PingPopupView(_viewModel: ping)
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ping = viewModel?.pings[indexPath.item]
        showPing(ping: ping!)

    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: collectionView.frame.width/2 , bottom: 0.0, right: collectionView.frame.width/2)
    }
}
