////
////  ContactListHeaderView.swift
////  The-Mouve-iOS
////
////  Created by Hilal Habashi on 11/24/16.
////  Copyright © 2016 Hilal Habashi. All rights reserved.
////
import UIKit
import Nuke
class ContactListHeaderView: UIView {
    let topSpace = UIApplication.shared.statusBarFrame.height
    var headerImageView: UIImageView!
    var profileImageView: UIImageView!
    var subtitleLabel: UILabel!
    var titleLabel: UILabel!
    var leftButtonView: UIImageView?
    var rightButtonView: UIImageView?
    var delegate: ContactListHeaderViewDelegate?

    convenience init(viewModel: ProfileViewModel) {
        self.init()
        self.setupView(bgColor: kThemeMainColor, picURL: viewModel.picURL, title: viewModel.fullName, subtitle: viewModel.username)
    }
    convenience init(bgColor: UIColor, picURL: URL, title: String, subtitle: String){
        self.init()
        self.setupView(bgColor: kThemeMainColor, picURL: picURL, title: title, subtitle: subtitle)
    }
    
    func setupView(bgColor: UIColor, picURL: URL, title: String, subtitle: String){
        self.profileImageView = setupProfileImage(image: picURL)
        self.addSubview(self.profileImageView!)
        profileImageView?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top).offset(topSpace + 5)
            make.width.equalTo(self.snp.width).dividedBy(3)
            make.height.equalTo(self.snp.width).dividedBy(3)
            
        }
        self.titleLabel = setupTitle(text: title)
        self.addSubview(self.titleLabel!)
        titleLabel?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.profileImageView.snp.bottom).offset(5)
            make.height.equalTo(25)
            make.width.equalTo(self.snp.width).multipliedBy(0.6)
        }
        self.subtitleLabel = setupSubtitle(text: subtitle)
        self.addSubview(self.subtitleLabel!)
        subtitleLabel?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            make.height.equalTo(10)
            make.width.equalTo(100)
        }
        self.headerImageView = setupHeaderImageView(color: bgColor)
        self.insertSubview(self.headerImageView!, belowSubview: self.profileImageView!)
        headerImageView?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top)
            make.height.equalTo(self.profileImageView.snp.height).dividedBy(2).offset(topSpace + 5)
            make.width.equalTo(self.snp.width)
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.layer.frame.width / 2
    }
    func setupProfileImage(image: URL) -> UIImageView{
        let imageView = UIImageView()
        
        Nuke.loadImage(with: image, into: imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = imageView.bounds.width / 2
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = kThemeMainColor.darken(byPercentage: 0.3).cgColor
        imageView.clipsToBounds = true
        
        return imageView
    }
    
    func setupTitle(text: String) -> UILabel{
        let label = UILabel()
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.baselineAdjustment = .alignCenters
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        
        return label
    }
    
    func setupSubtitle(text: String) -> UILabel{
        let label = UILabel()
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
        label.baselineAdjustment = .alignCenters
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor.lightGray
        
        return label
    }
    
    
    func setupHeaderImageView(color: UIColor) -> UIImageView{
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = color
        return imageView
    }
    
    func setButtons(rightImage: UIImage? = nil, leftImage: UIImage? = nil){
        if(rightImage != nil){
            rightButtonView = UIImageView(image: rightImage?.withRenderingMode(.alwaysTemplate))
            
            rightButtonView?.frame = CGRect(x: ((self.frame.width - 5) - 32), y: UIApplication.shared.statusBarFrame.height + 5, width: 28, height: 28)
            rightButtonView?.clipsToBounds = true
            rightButtonView?.tintColor = UIColor.white
            rightButtonView?.contentMode = .scaleAspectFit
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(rightButtonTapped))
            rightButtonView?.isUserInteractionEnabled = true
            rightButtonView?.addGestureRecognizer(tapGestureRecognizer)
            self.addSubview(rightButtonView!)
        }
        if(leftImage != nil){
            leftButtonView = UIImageView(image: leftImage?.withRenderingMode(.alwaysTemplate))
            leftButtonView?.frame = CGRect(x: 9, y: UIApplication.shared.statusBarFrame.height + 5, width: 32, height: 32)
            leftButtonView?.clipsToBounds = true
            leftButtonView?.contentMode = .scaleAspectFit
            leftButtonView?.tintColor = UIColor.white
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(leftButtonTapped))
            leftButtonView?.isUserInteractionEnabled = true
            leftButtonView?.addGestureRecognizer(tapGestureRecognizer)
            self.addSubview(leftButtonView!)
        }
    }
    
    func leftButtonTapped(){
        log.info("Left Button Clicked")
        self.delegate?.leftButtonTapped()
    }
    func rightButtonTapped(){
        log.info("Right Button Clicked")
        self.delegate?.rightButtonTapped()
    }
}

protocol ContactListHeaderViewDelegate{
    func leftButtonTapped()
    func rightButtonTapped()
}
