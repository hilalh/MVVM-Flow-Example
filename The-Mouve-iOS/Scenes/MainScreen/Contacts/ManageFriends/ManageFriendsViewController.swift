//
//  ManageFriendsViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import PagingMenuController

class ManageFriendsViewController: UIViewController {

    struct Item1: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            let title = MenuItemText(text: "Invite Friends", color: kThemeMainColor, selectedColor: kThemeMainColor, font: UIFont.systemFont(ofSize: 12.0), selectedFont: UIFont.systemFont(ofSize: 12.0))

            return .text(title: title)
        }
    }
    struct Item2: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            let title = MenuItemText(text: "Add Back", color: kThemeMainColor, selectedColor: kThemeMainColor, font: UIFont.systemFont(ofSize: 12.0), selectedFont: UIFont.systemFont(ofSize: 12.0))
            return .text(title: title)
        }
    }

    struct MenuOptions: MenuViewCustomizable {
        var backgroundColor: UIColor = UIColor.white
        var height: CGFloat = 44
//        var menuSelectedItemCenter: Bool = true
        var focusMode: MenuFocusMode = .underline(height: 2.0, color: kThemeMainColor, horizontalPadding: 40.0, verticalPadding: 10.0)
        var displayMode: MenuDisplayMode = .segmentedControl
        var itemsOptions: [MenuItemViewCustomizable] {
            return [Item1(), Item2()]
        }
    }

    struct PagingMenuOptions: PagingMenuControllerCustomizable {
        var isScrollEnabled: Bool = false
        var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: [AddInviteFriendsViewController(), UIViewController()]
            )
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "cross-cancel-icon"), style: .plain, target: self, action:#selector(dismissVC))
        setNavBar(title: "Manage Friends", leftButton: leftButton)

        self.edgesForExtendedLayout = []

        let options = PagingMenuOptions()
        let pagingMenuController = PagingMenuController(options: options)

        addChildViewController(pagingMenuController)
        view.addSubview(pagingMenuController.view)

        pagingMenuController.didMove(toParentViewController: self)



        // Do any additional setup after loading the view.
    }

    func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
