//
//  ContactListDataManager.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/9/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke
import RxSwift
import RxCocoa

class ContactListDataManager: NSObject {
    private var user: abstractUser
    var friends: [ContactViewModel] = []
    var pings: [PingViewModel] = []
    var userViewModel: ProfileViewModel{
        return ProfileViewModel(user: user)
    }
    /*
    var fullName: String {
        return user.fullName
    }
    var userName: String {
        return user.username
    }
    var picURL: URL {
        return URL(string: user.profileImageURL!)!
    }*/
    
    
    init(user: abstractUser) {
        self.user = user
        super.init()
        fetchFriends()
        fetchPings()
    }
    
    /// Fetch contacts for the given user
    func fetchFriends(){
        for friend in dataManager.fakeData.users{
            self.friends.append(ContactViewModel(user: friend))
        }
    }
    
    func fetchPings(){
        for ping in dataManager.fakeData.pings{
            self.pings.append(PingViewModel(ping: ping))
        }
    }
}
