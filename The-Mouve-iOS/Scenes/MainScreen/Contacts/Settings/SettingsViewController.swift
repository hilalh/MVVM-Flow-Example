//
//  SettingsViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Eureka

class SettingsViewController: FormViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setForm()
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "cross-cancel-icon"), style: .plain, target: self, action:#selector(dismissVC))
        setNavBar(title: "Settings", leftButton: leftButton)

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setForm(){
        form
            +++ Section("General")
            <<< LabelRow(){
                $0.title = "Coming Soon"
            }
            +++ Section("Notifications")
            <<< SwitchRow(){
                $0.title = "Friendship"
                $0.value = true
            }
            <<< SwitchRow(){
                $0.title = "Invites"
                $0.value = true
            }
            <<< SwitchRow(){
                $0.title = "Pings"
                $0.value = true
            }
            +++ Section("Account")
            <<< ButtonRow(){
                $0.title = "Edit Details"
                $0.onCellSelection{(cell, row) in
                    let vc = SignUpViewController()
                    self.present(vc , animated: true, completion: nil)
                }

            }
            <<< ButtonRow(){
                $0.title = "Log Out"
                $0.onCellSelection{(cell, row) in
                    dataManager.userOps.logOut()
                }
            }
            <<< ButtonRow(){
                $0.title = "Delete Account"
                $0.onCellSelection{(cell, row) in
                    dataManager.userOps.deleteAccount()
                }
            }
            +++ Section("Links & Terms")
            <<< ButtonRow(){
                $0.title = "Privacy Statement"
            }
            <<< ButtonRow(){
                $0.title = "Terms & Conditions"
            }
            <<< ButtonRow(){
                $0.title = "Libraries & Frameworks"
            }

    }

    func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
