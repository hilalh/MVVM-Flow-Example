//
//  ProfileViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/8/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

class ProfileViewModel: NSObject {
    private var user: abstractUser
    var fullName: String{
        return user.fullName
    }
    var username: String{
        let un = user.fullName.lowercased().replacingOccurrences(of: " ", with: "")
        return "@\(un)"
    }
    var picURL: URL{
        return URL(string: user.profileImageURL!)!
    }
    
    init(user: abstractUser){
        self.user = user
    }
    
}
