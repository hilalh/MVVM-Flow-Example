//
//  HomeViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/8/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import ChameleonFramework
import PagingMenuController

class HomeViewController: UIViewController {


    struct Item1: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            let title = MenuItemText(text: "All",  color: kThemeMainColor, selectedColor: kThemeMainColor, font: UIFont.systemFont(ofSize: 12.0), selectedFont: UIFont.systemFont(ofSize: 12.0))

            return .text(title: title)
        }
    }
    struct Item2: MenuItemViewCustomizable {
        var displayMode: MenuItemDisplayMode {
            let title = MenuItemText(text: "Going To",color: kThemeMainColor, selectedColor: kThemeMainColor, font: UIFont.systemFont(ofSize: 12.0), selectedFont: UIFont.systemFont(ofSize: 12.0))
            return .text(title: title)
        }
    }

    struct MenuOptions: MenuViewCustomizable {
        var backgroundColor: UIColor = UIColor.white
        var height: CGFloat = 44
        var focusMode: MenuFocusMode = .underline(height: 2.0, color: kThemeMainColor, horizontalPadding: 40.0, verticalPadding: 10.0)
        var displayMode: MenuDisplayMode = .segmentedControl
        var itemsOptions: [MenuItemViewCustomizable] {
            return [Item1(), Item2()]
        }
    }

    struct PagingMenuOptions: PagingMenuControllerCustomizable {
        var isScrollEnabled: Bool = false
        var componentType: ComponentType {
            return .all(menuOptions: MenuOptions(), pagingControllers: [ScenePageViewController(viewMode: .allMouves), ScenePageViewController(viewMode: .checkedInMouves)])
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.edgesForExtendedLayout = []
        self.extendedLayoutIncludesOpaqueBars = false
        self.automaticallyAdjustsScrollViewInsets = false

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStatusBarStyle(.default)

        let options = PagingMenuOptions()
        let pagingMenuController = PagingMenuController(options: options)

        addChildViewController(pagingMenuController)
        view.addSubview(pagingMenuController.view)

        pagingMenuController.didMove(toParentViewController: self)



        // Do any additional setup after loading the view.

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
