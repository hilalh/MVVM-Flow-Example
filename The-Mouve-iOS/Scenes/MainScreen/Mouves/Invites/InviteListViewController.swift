//
//  InviteViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/6/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Foundation
//import RxSwift
//import RxCocoa

class InviteListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var backButton: UIBarButtonItem!
    var createMouve: UIBarButtonItem!
    var tableView: UITableView?
    var viewModel: InviteListDataManager?


    convenience init(_vM: InviteListDataManager) {
        self.init()
        self.viewModel = _vM
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView = UITableView(frame: UIScreen.main.bounds, style: UITableViewStyle.plain)
        tableView?.delegate = self
        tableView?.dataSource = self
//        tableView?.tableHeaderView = TableHeader()
        self.view.backgroundColor = kThemeMainColor
        self.view.addSubview(tableView!)

        tableView?.register(ContactsTableViewCell.self, forCellReuseIdentifier: "DefaultCell")
    }

    override func viewDidAppear(_ animated: Bool) {
    }

    override func viewWillAppear(_ animated: Bool) {
      backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "cross-cancel-icon"), style: .plain, target: self, action: #selector(backButtonTapped))
      createMouve = UIBarButtonItem(title: "Create", style: .plain, target: self, action: #selector(createMouveTapped))

      setNavBar(title: "Invite", leftButton: backButton, rightButton: createMouve)
    }

    override func viewWillDisappear(_ animated: Bool) {

    }

    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }

    func backButtonTapped(){
        self.modalTransitionStyle = .crossDissolve
        self.dismiss(animated: true, completion: nil)
        //        self.navigationController!.popViewController(true)
        log.info("Add person tapped")
    }

    func createMouveTapped(){
        log.info("Settings tapped")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ContactsTableViewCell?
        let friend = viewModel?.friends[indexPath.section]
        cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath as IndexPath) as? ContactsTableViewCell
        cell?.inviteCell(viewModel: friend!)
        if cell == nil {
            cell = ContactsTableViewCell.inviteCell(viewModel: friend!)
        }

        return cell!
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60

    }
}
