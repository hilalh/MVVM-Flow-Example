//
//  InviteViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/5/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class InviteListDataManager {
    
    var mouve: FMouve!
    var friends: [ContactViewModel]{
        return fetchFriends()
    }
    var chosenPeople: [FUser]!

    init(_mouve: FMouve) {
        mouve = _mouve
    }
    func fetchFriends() -> [ContactViewModel]{
        return dataManager.fakeData.users.map{ContactViewModel(user: $0)}
    }
    func inviteOthers(_ _people: [FUser]) {
//        TODO: Finish the actual invite others function
        dataManager.shared.invitePeopleToMouve(self.mouve, people: _people)
    }



}
