//
//  CreateMouveProcessViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/18/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import SwiftDate
import CoreLocation
import RxCocoa
import RxSwift

struct MouveTime{
    var startTime: Date
    var endTime: Date
}

class CreateMouveProcessViewModel: NSObject {
    var descriptionText: BehaviorSubject<String>
    var locationLat: BehaviorSubject<String>
    var locationLng: BehaviorSubject<String>
    var allowFriendsInvite: BehaviorSubject<Bool>
    var startTime: BehaviorSubject<String>
    var endTime: BehaviorSubject<String>
    var thumbnailURL: BehaviorSubject<URL>
    var videoURL: BehaviorSubject<URL>
    override init() {
        descriptionText = BehaviorSubject(value: "")
        locationLat = BehaviorSubject(value: "")
        locationLng = BehaviorSubject(value: "")
        allowFriendsInvite = BehaviorSubject(value:false)
        startTime = BehaviorSubject(value: "")
        endTime = BehaviorSubject(value: "")
        videoURL = BehaviorSubject(value: URL(string: "")!)
        thumbnailURL = BehaviorSubject(value: URL(string: "")!)
    }
    
    func uploadMouveFiles(_ mouve: FMouve) {
        let thumbTask = dataManager.shared.uploadMouveFile(name: "thumb", format: "jpeg", contentType: "image", key: mouve.key, fileURL: URL(string: mouve.thumbnailImgURL!)!){(metadata, error) in
            //Set URL here
            //videoURL.value() = metadata?.downloadURL()!
            //mouve.thumbnailImgURL = metadata?.downloadURL()
        }
        let videoTask = dataManager.shared.uploadMouveFile(name: "video", format: "mp4", contentType: "video", key: mouve.key, fileURL: URL(string: mouve.videoURL!)!){(metadata, error) in
            
            //mouve.videoURL = metadata?.downloadURL()
            //Set URL here
        }
    }
    
    func setTime(mouveTime: MouveTime){
        startTime = BehaviorSubject(value: DateInRegion(absoluteDate: mouveTime.startTime).iso8601())
        endTime = BehaviorSubject(value: DateInRegion(absoluteDate: mouveTime.endTime).iso8601())
    }
    
    func setLocation(loc: CLLocation){
        locationLng = try! BehaviorSubject(value: loc.coordinate.longitude.string(options: nil, shared: nil))
        locationLat = try! BehaviorSubject(value: loc.coordinate.latitude.string(options: nil, shared: nil))
    }
    
    func setDescription(desc: String){
        descriptionText = BehaviorSubject(value: desc)
    }
    
    func setInviteFriendsBool(allowed: Bool){
        allowFriendsInvite = BehaviorSubject(value: allowed)
    }
}
