//
//  CreateMouveViewController.swift
//  The-Mouve-iOS
//
//  Created by Samuel Ojogbo on 10/10/16.
//  Copyright © 2016 Samuel Ojogbo. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift
import RxCocoa
import EZSwipeController
import Eureka
import LocationPickerViewController
import LLSimpleCamera

class CreateMouveProcessViewController: EZSwipeController {
    
    var delegate: CreateMouveProcessDelegate?
    var VCs: [UIViewController]?
    var rightButton: UIBarButtonItem?
    var leftButton: UIBarButtonItem?
//    View-model stuff
    var viewModel: CreateMouveProcessViewModel?
    let disposeBag = DisposeBag()
//    To-be tied
    var descriptionText: String?
    var locationPoint: CLLocation?
    var friendInviteBool: Bool = false
    var mouveTime: MouveTime?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard viewModel != nil else {
            return
        }
        
//        viewModel.allowFriendsInvite
        

        
    }
    func disablePageScroll(){
        for view: UIView in pageViewController.view.subviews {
            guard let scrollView = view as? UIScrollView else{
                log.error("This is not a scroll view")
                return
            }
            scrollView.delegate = self
            scrollView.isScrollEnabled = false
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        disablePageScroll()
    }
    override func setupView() {
        datasource = self
        cancelStandardButtonEvents = true
    }
    
    func dismissVC(){
        self.delegate?.dismissVC()
    }
    
    
    func nextVC(){
        switch self.currentVCIndex {
        case 0:
                log.info("Move to next page...")
                moveToPage(self.currentVCIndex+1, animated: true)
        case 1:
                moveToPage(self.currentVCIndex+1, animated: true)
        case 2:
                log.info("Create A Mouve Process is Done")
                self.presentCamera()
        default:
            log.debug("Do Nothing")
            
            
        }
    }
    
    func lastVC(){
        switch self.currentVCIndex {
        case 0:
            log.info("Cancel Make Mouve Process")
            self.dismissVC()
        default:
            moveToPage(self.currentVCIndex-1, animated: true)
        }
    }

    func presentCamera(){
        let camera = CameraViewController()
        self.present(UINavigationController(rootViewController: camera), animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func DescriptionForm() -> UIViewController{
        let vc = FormViewController()
        vc.form =
            Section("Invite Options")
            <<< SwitchRow(){ row in
                row.title = "Friends can invite anyone?"
                row.cell.switchControl?.onTintColor = UIColor.lightGray
                row.cell.switchControl?.tintColor = UIColor.lightGray
                row.cell.switchControl?.thumbTintColor = UIColor.gray
                
            }
            
            +++ Section("What's Happening?")
            <<< TextAreaRow(){ row in
                row.add(rule: RuleRequired())
                row.validationOptions = .validatesAlways
                row.placeholder = "Enter description here"
                }.onRowValidationChanged() { cell, row in
                    if(row.validationErrors.count == 0){
                        log.info("Now valid")
                        self.rightButton?.isEnabled = true
                    }
                    else{
                        log.info("Non valid")
                    }
                }
        vc.tableView?.backgroundColor = UIColor.white
        return vc
    }
    func presentAlertBox(msg: String){
        let alert = UIAlertController(title: "Oops...", message: "\(msg)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func LocationPickerForm() -> UIViewController{
        let vc = LocationPicker()
        vc.searchBar.backgroundColor = UIColor.white
        vc.searchBar.barTintColor = UIColor.white
        vc.pickCompletion = { (pickedLocationItem) in
            // Do something with the location the user picked.
        }
        return vc
    }
    
    
    func TimeForm() -> UIViewController{
        let vc = FormViewController()
        vc.form = Section()
            <<< DateTimeRow(){ row in
                row.title = "From?"
                row.minimumDate = Date()
            }
            
            +++ Section()
            <<< DateTimeRow(){ row in
                row.title = "To?"
                row.minimumDate = Date()
        }
        return vc
    }
}
extension CreateMouveProcessViewController: EZSwipeControllerDataSource, UIScrollViewDelegate {
    func viewControllerData() -> [UIViewController] {
        self.VCs = [DescriptionForm(), LocationPickerForm(), TimeForm()]
        return self.VCs!
    }
    func generateNavBar() -> UINavigationBar{
        let navigationBar = UINavigationBar()
        navigationBar.isTranslucent = false
        navigationBar.barStyle = UIBarStyle.default
        navigationBar.barTintColor = kThemeMainColor
        navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationBar.shadowImage = UIImage()
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navigationBar.pushItem(navigationItem, animated: false)
        return navigationBar

    }
    /*
    func disableSwipingForLeftButtonAtPageIndex(_ index: Int) -> Bool {
        return true
    }
    func disableSwipingForRightButtonAtPageIndex(_ index: Int) -> Bool {
        return true
    }
    */
    func navigationBarDataForPageIndex(_ index: Int) -> UINavigationBar{
        let navBar = generateNavBar()
        let navItem = UINavigationItem()
        switch index {
        case 0:
            navItem.title = "What?"
            leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "cross-cancel-icon"), style: .plain, target: self, action: #selector(lastVC))
            rightButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextVC))
        case 1:
            navItem.title = "Where?"
            leftButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(lastVC))
            rightButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextVC))
        case 2:
            navItem.title = "When?"
            leftButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(lastVC))
            rightButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextVC))
        default:
            log.info("Nothing")
        }
        navItem.setLeftBarButton(leftButton, animated: true)
        navItem.setRightBarButton(rightButton, animated: true)
        //rightButton?.isEnabled = false
        navItem.rightBarButtonItem?.tintColor = UIColor.white
        navItem.leftBarButtonItem?.tintColor = UIColor.white
        navBar.pushItem(navItem, animated: true)
        return navBar
    }
}

protocol CreateMouveProcessDelegate {
    func dismissVC()
}

