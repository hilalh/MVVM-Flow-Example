//
//  CreateMouveViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Eureka

class CreateMouveViewController: UIViewController {

    
    var cancelButton: UIBarButtonItem?
    var controller = CreateMouveProcessViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Hide navigationController navbar to use UINavigationBar as standalone item
        navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = kThemeMainColor
        self.addChildViewController(controller)
        controller.view.frame = CGRect(x: 0, y: 20, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - 20)
        self.view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
        controller.delegate = self


        // Do any additional setup after loading the view.
    }
    
    
    func exitVC(){
        self.dismiss(animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CreateMouveViewController: CreateMouveProcessDelegate{
    func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    func segueToInviteVC(){
        let vc = InviteListViewController()
        
        vc.modalTransitionStyle = .crossDissolve
        
        
        let navVC = UINavigationController(rootViewController: vc)
        self.navigationController?.present(navVC, animated: true, completion: nil)
        
    }
    func segueToCameraVC(){
        let storyboard = UIStoryboard(name: "Camera", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "recordMouveVideo") as UIViewController
        self.navigationController?.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
