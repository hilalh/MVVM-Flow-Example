//
//  ScenePageViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/20/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import XLActionController
import DZNEmptyDataSet
import LTInfiniteScrollViewSwift

class ScenePageViewController: UIViewController {
    
//    @IBOutlet var collectionView: UICollectionView!
    //View Model of the whole card stack
    var viewModel: ScenePageDataManager!
    //View Model of a certain card
    var mouveViewModel: MouveCardViewModel?
    var scrollView: LTInfiniteScrollView?
    let screenWidth = UIScreen.main.bounds.size.width

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        setCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavBar(title: "")
        scrollView?.reloadData(initialIndex: 0)
    }
    
    
    func setCollectionView(){
        scrollView = LTInfiniteScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        //        self.edgesForExtendedLayout = []
        //        self.extendedLayoutIncludesOpaqueBars = false
        //        self.automaticallyAdjustsScrollViewInsets = false
        scrollView?.center = self.view.center
        view.addSubview(scrollView!)
        scrollView?.dataSource = self
        scrollView?.delegate = self
        scrollView?.maxScrollDistance = 1
    }
    
    convenience init(viewMode: ScenePageModes) {
        self.init()
        self.viewModel = ScenePageDataManager(_viewMode: viewMode)
    }
    

    
    func generateCard(index: Int, frame: CGRect) -> UIView{
        mouveViewModel = viewModel.mouves[index]
        let card = MouveCardViewController(_viewModel: mouveViewModel!, frame: frame)
        card.frame.size.width = frame.width * 0.9
        card.frame.size.height = frame.height * 0.65
        card.center = (scrollView?.center)!
        card.delegate = self
        card.backgroundColor = UIColor.white
        card.layer.borderWidth = CGFloat(0.5)
        card.layer.borderColor = UIColor.darkGray.cgColor
        card.layer.cornerRadius = 3
        card.layer.masksToBounds = true
        return card
    }
    


}

extension ScenePageViewController:  DZNEmptyDataSetSource, DZNEmptyDataSetDelegate{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "Ain't no Mouves")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "Stop being lazy! Go make some Mouves bruh!")
    }
}

extension ScenePageViewController: LTInfiniteScrollViewDataSource {
    
    func viewAtIndex(_ index: Int, reusingView view: UIView?) -> UIView {
        return generateCard(index: index ,frame: (self.scrollView?.frame)!)
    }
    
    func numberOfViews() -> Int {
        return viewModel.mouves.count
    }
    
    func numberOfVisibleViews() -> Int {
        return 1
    }
}

extension ScenePageViewController: LTInfiniteScrollViewDelegate {
    
    func updateView(_ view: UIView, withProgress progress: CGFloat, scrollDirection direction: LTInfiniteScrollView.ScrollDirection) {
    }
    
    func scrollViewDidScrollToIndex(_ scrollView: LTInfiniteScrollView, index: Int) {
        log.info("scroll to index: \(index)")
    }
}

extension ScenePageViewController: MouveCardViewDelegate{
    func openActionSheet(){
        let actionController = MouveActionController()
        actionController.headerData = "What would you like to do?"
        actionController.addAction(Action("Invite more people", style: .default, handler: { action in
            log.info("Invite to Mouve")
            self.show(UINavigationController(rootViewController: InviteListViewController()), sender: self)
        }))
        actionController.addAction(Action("Report this Mouve", style: .destructive, handler: { action in
            self.mouveViewModel?.reportMouve()
        }))
        actionController.addAction(Action("Cancel", style: .cancel, handler:nil))
        
        self.present(actionController, animated: true, completion: nil)
    
    }
    func playVideo(){
        self.present(PreviewVideoViewController(videoUrl: (self.mouveViewModel?.video)!), animated: true, completion: nil)
    }
    
    func expandView() {
        log.info("Expand Button tapped")
        let vc = ExpandedViewController(_viewModel: self.mouveViewModel!)
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
}
