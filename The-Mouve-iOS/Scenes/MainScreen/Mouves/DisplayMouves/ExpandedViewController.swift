//
//  ExpandedViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/9/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import XLActionController

class ExpandedViewController: UIViewController {
    var viewModel: MouveCardViewModel?
    var detailedView: UIView = UIView()
    var goingCollctionView: ScrollablePeopleTableViewCell?
    var regularView: MouveCardViewController?
    
    convenience init(_viewModel: MouveCardViewModel) {
        self.init()
        self.viewModel = _viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCardView(viewModel: self.viewModel!)
        setExitButton()
        setExpandButton()
        setGoingView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setCardView(viewModel: MouveCardViewModel){
        self.view.backgroundColor = kThemeBrighterColor.withAlphaComponent(1)
        regularView = MouveCardViewController(_viewModel: viewModel)
        self.view.addSubview(regularView!)
        regularView?.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(self.view.snp.width).multipliedBy(0.9)
            make.height.equalTo(self.view.snp.height).multipliedBy(0.85)
            //make.height.equalTo(self.view.snp.height).multipliedBy(0.55)
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(self.view.snp.top).offset(50)
        }
        regularView?.layer.backgroundColor = UIColor.white.cgColor
        regularView?.layer.borderWidth = CGFloat(0.5)
        regularView?.layer.borderColor = UIColor.darkGray.cgColor
        regularView?.layer.cornerRadius = 3
        regularView?.layer.masksToBounds = true
        regularView?.delegate = self
        
    }

    func setExpandButton(){
        /*
        regularView?.expandButton?.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo((regularView?.snp.width)!).multipliedBy(1)
            make.height.equalTo((regularView?.snp.height)!).multipliedBy(0.1)
            make.centerX.equalTo((regularView?.snp.centerX)!)
            make.top.greaterThanOrEqualTo((regularView?.invitedByLabel?.snp.bottom)!)
            //make.bottom.lessThanOrEqualTo((goingCollctionView?.snp.top)!)
        }
        regularView?.expandButton?.setTitleColor(UIColor.black, for: .normal)
        regularView?.expandButton?.setTitleColor(kThemeMainColor, for: .normal)
        regularView?.expandButton?.setTitle("^ Collapse ^", for: .normal)
        regularView?.expandButton?.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        regularView?.bringSubview(toFront: (regularView?.expandButton)!)
        regularView?.expandButton?.layer.borderColor = UIColor.flatRed().cgColor
         */
        regularView?.expandButton?.removeFromSuperview()
    }

    func setGoingView(){
        
        goingCollctionView = ScrollablePeopleTableViewCell()
        regularView?.addSubview(goingCollctionView!)
        goingCollctionView?.setCollectionView()
        goingCollctionView?.headerLabel.text = "People Going"
        goingCollctionView?.headerLabel.textAlignment = .left
        goingCollctionView?.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo((regularView?.snp.width)!)
            make.height.equalTo((regularView?.snp.height)!).multipliedBy(0.15)
            make.left.equalTo((regularView?.locationLabel?.snp.left)!)
            make.top.greaterThanOrEqualTo((regularView?.invitedByLabel?.snp.bottom)!)
            make.bottom.lessThanOrEqualTo((regularView?.checkInBttn?.snp.top)!)
        }
        goingCollctionView?.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: 0, cellIdentifier: "AttendeeCell")
    }
    
    func setExitButton(){
        let exitButton = UIButton()
        self.view.addSubview(exitButton)
        exitButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(self.view.snp.width).multipliedBy(0.1)
            make.height.equalTo(self.view.snp.width).multipliedBy(0.1)
            make.bottom.equalTo(self.view.snp.bottom).inset(5)
            make.top.greaterThanOrEqualTo((regularView?.snp.bottom)!).inset(5)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        exitButton.setImage(#imageLiteral(resourceName: "slickX").withRenderingMode(.alwaysTemplate), for: .normal)
        exitButton.tintColor = UIColor.white

        exitButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
    }
    
    
    func dismissVC(){
        UIView.animate(withDuration: 0.3, animations: {
            self.regularView?.frame.size.height = self.view.frame.height * 0.6
            self.view.layoutIfNeeded()
        },
       completion: { completed in
            self.dismiss(animated: true, completion: nil)
        })
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ExpandedViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        
        let attendees = viewModel?.peopleGoing.count
        log.info("Attendees: \(attendees!)")
        return (attendees!)
 
        //return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttendeeCell",
                                                      for: indexPath) as? PeopleCollectionViewCell
        let attendee = viewModel?.peopleGoing[indexPath.item]
        cell?.setCell(viewModel: attendee!)
        
        return cell!
    }
}

extension ExpandedViewController: MouveCardViewDelegate{
    func openActionSheet(){
        let actionController = MouveActionController()
        actionController.headerData = "What would you like to do?"
        actionController.addAction(Action("Invite more people", style: .default, handler: { action in
            log.info("Invite to Mouve")
            self.show(UINavigationController(rootViewController: InviteListViewController()), sender: self)
        }))
        actionController.addAction(Action("Report this Mouve", style: .destructive, handler: { action in
            self.viewModel?.reportMouve()
        }))
        actionController.addAction(Action("Cancel", style: .cancel, handler:nil))
        
        self.present(actionController, animated: true, completion: nil)
        
    }
    func playVideo(){
        //let videoUrl: URL = URL(string: (self.viewModel?.video)!)!
        let player = PreviewVideoViewController(videoUrl: (self.viewModel?.video)!)
        self.present(player, animated: true, completion: nil)
    }
    
    func expandView() {
        
        log.info("This view cannot be expanded")
        

        
    }
    
}

