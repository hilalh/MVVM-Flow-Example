//
//  AppDelegate.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/3/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Firebase
import FacebookCore
import Fabric
import Branch
import DigitsKit
import TwitterKit
import Crashlytics
import SwiftyBeaver

let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FIRApp.configure()
        // Override point for customization after application launch.
        Fabric.with([Digits.self, Twitter.self, Crashlytics.self])
        FacebookCore.SDKApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: [
            .sourceApplication: application,
            ])
        
        let loggingDest = SBPlatformDestination(appID: "XWxxog", appSecret: "4nxey8qy9cg0iezqrYauagufJEtdYXiK", encryptionKey: "wGJeo7H6jnplHE7icqrUr3TrSXaamxif")
        log.addDestination(loggingDest)

        var initialViewController: UIViewController?
        initialViewController = LoginViewController()
        self.window!.rootViewController = initialViewController
        self.window!.makeKeyAndVisible()
 

        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        SDKApplicationDelegate.shared.application(app, open: url, options: options)
        return true
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }


    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        AppEventsLogger.activate(application)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}
