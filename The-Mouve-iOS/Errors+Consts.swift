//
//  Errors+Consts.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Foundation
let kErrorUsernameAlreadyTaken = "Username already taken"
let kErrorSetUsername = "Couldn't set username"

enum kUserError: String{
    case kErrorUsernameAlreadyTaken = "Username already taken"
    case kErrorSetUsername = "Couldn't set username"
}

enum UserError: Error{
    case usernameAlreadyTaken
    case phoneFieldIsEmpty
    case usernameFieldIsEmpty
}
//Object Errors
enum kObjectError: String{
    case kErrorUnboxJSON = "Cannot Unbox JSON"
    case kErrorEmptyDict = "Dictionary is empty"
}

enum ObjectError: Error{
    case cannotUnboxObject
    case dictIsEmpty
}

let kErrorUnboxJSON = "Cannot Unbox JSON"
let kErrorEmptyDict = "Dictionary is empty"

//
enum kFacebookError: String{
    case kErrorGraphRequest = "Cannot complete Facebook Graph request"
}

enum FacebookError: Error{
    case graphRequestFailed
    case loginProcessFailed
}
let kErrorGraphRequest = "Cannot complete Facebook Graph request"
