//
//  MouveCardViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/11/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke
import XLActionController
import SnapKit
import Foundation
import RxSwift
import RxCocoa

//TODO: Solve the breaking autolayout

class MouveCardViewController: UIView {
    
    //Const
    var checkInBttn: UIButton?
    var moreBttn: UIButton?
    var playButton: UIButton?
    var invitedByLabel: UILabel?
    var expandButton: UIButton?
    //Vars
    var titleLabel: UILabel?
    var timeLabel: UILabel?
    var locationLabel: UILabel?
    var attendeesCollView: UIView?
    var thumbnailDisplay: UIImageView?
    weak var viewModel: MouveCardViewModel!
    var delegate: MouveCardViewDelegate?
    
    class func initCard(titleText: String, timeText: String, addressText: String, picURL: URL ,frame: CGRect) -> UIView{
        let view = MouveCardViewController(frame: frame)
        view.setupView(titleText: titleText, timeText: timeText, addressText: addressText, picURL: picURL, peopleGoing: [ContactViewModel]())
        return view
    }
    func setThumbnailView(image: URL){
        thumbnailDisplay = UIImageView()
        Nuke.loadImage(with: image, into: thumbnailDisplay!)
        self.addSubview(thumbnailDisplay!)
        
        thumbnailDisplay?.snp.makeConstraints{ (make) -> Void in
            make.left.greaterThanOrEqualTo(self.snp.left).offset(10)
            //make.top.greaterThanOrEqualTo(self.snp.top).inset(10)
            make.top.equalTo(self.snp.top).inset(10)
//            make.height.equalTo(self.snp.height).multipliedBy(0.45)
            make.height.equalTo(self.snp.width).multipliedBy(0.70)
            make.width.equalTo(self.snp.width).multipliedBy(0.90)
            make.centerX.equalTo(self.snp.centerX)
        }
        
        thumbnailDisplay?.contentMode = .scaleAspectFill
        thumbnailDisplay?.layer.cornerRadius = 2
        thumbnailDisplay?.clipsToBounds = true
        thumbnailDisplay?.layer.masksToBounds = true
        
        playButton = UIButton()
        playButton?.setImage((image: #imageLiteral(resourceName: "Circled Play").withRenderingMode(.alwaysTemplate)), for: .normal)
        self.addSubview(playButton!)
        playButton?.snp.makeConstraints{ (make) -> Void in
            make.height.equalTo((thumbnailDisplay?.snp.width)!).dividedBy(4)
            make.width.equalTo((thumbnailDisplay?.snp.width)!).dividedBy(4)
            make.center.equalTo((thumbnailDisplay?.snp.center)!)
        }
        playButton?.tintColor = kThemeBrighterColor
        playButton?.addTarget(self, action: #selector(playButtonTapped(_sender:)), for: .touchUpInside)
        setMoreButton()
        /*
        thumbnailDisplay?.backgroundColor = kThemeBrighterColor
        thumbnailDisplay?.layer.shadowColor = UIColor.darkGray.cgColor
        thumbnailDisplay?.layer.shadowOffset = CGSize(width: 1, height: 1)
        thumbnailDisplay?.layer.shadowOpacity = 0.7
        thumbnailDisplay?.layer.shadowPath = UIBezierPath(rect: (thumbnailDisplay?.bounds)!).cgPath
        */
    }
    
    func setTitle(text: String){
        titleLabel = UILabel()
        self.addSubview(titleLabel!)
        
        titleLabel?.snp.makeConstraints{ (make) -> Void in
            make.top.lessThanOrEqualTo((thumbnailDisplay?.snp.bottom)!).offset(5)
            make.left.lessThanOrEqualTo((thumbnailDisplay?.snp.left)!)
            //make.height.lessThanOrEqualTo(15)
            make.height.equalTo(15)
            make.width.equalTo(self.snp.width).multipliedBy(0.90)
        }
        titleLabel?.text = "\(text)"
        //titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.lineBreakMode = .byWordWrapping
        titleLabel?.numberOfLines = 0
    }
    
    func setTimeLabel(time: String){
        timeLabel = UILabel()
        self.addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints{ (make) -> Void in
            make.top.lessThanOrEqualTo((titleLabel?.snp.bottom)!).offset(5)
            make.left.lessThanOrEqualTo((titleLabel?.snp.left)!)
            //make.height.lessThanOrEqualTo(15)
            make.height.equalTo(15)
            make.width.lessThanOrEqualTo(self.snp.width).multipliedBy(0.90)
        }
        
        timeLabel?.text = "Time: \(time)"
        //timeLabel?.font = UIFont.systemFont(ofSize: 12)
        timeLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        timeLabel?.adjustsFontSizeToFitWidth = true
    }
    
    func setLocationLabel(location: String){
        locationLabel = UILabel()
        self.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints{ (make) -> Void in
            make.top.lessThanOrEqualTo((timeLabel?.snp.bottom)!).offset(5)
            make.left.lessThanOrEqualTo((timeLabel?.snp.left)!)
            //make.height.lessThanOrEqualTo(15)
            make.height.equalTo(15)
            make.width.lessThanOrEqualTo(self.snp.width).multipliedBy(0.90)
        }
        
        locationLabel?.text = "Location: \(location)"
        locationLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        locationLabel?.adjustsFontSizeToFitWidth = true
        locationLabel?.lineBreakMode = .byWordWrapping
        locationLabel?.numberOfLines = 0
        //locationLabel?.font = UIFont.systemFont(ofSize: 12)
    }
    
    func setMoreButton(){
        moreBttn = UIButton()
        self.addSubview(moreBttn!)
        //moreBtnView.frame = CGRect(x: (customView.frame.width - 64), y: 0, width: 44, height: 44)
        moreBttn?.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo((thumbnailDisplay?.snp.width)!).dividedBy(12)
            make.height.equalTo((thumbnailDisplay?.snp.height)!).dividedBy(36)
            make.top.equalTo((thumbnailDisplay?.snp.top)!).inset(2)
            make.right.equalTo((thumbnailDisplay?.snp.right)!).inset(2)
            
        }
        //moreBttn?.imageEdgeInsets = UIEdgeInsetsMake(12, 5, 26, 7.5)
        moreBttn?.setImage(#imageLiteral(resourceName: "xMore").withRenderingMode(.alwaysTemplate), for: .normal)
        moreBttn?.tintColor = kThemeBrighterColor
        moreBttn?.addTarget(self, action: #selector(moreButtonTapped(_sender:)), for: .touchUpInside)
    }
    
    func setGoButton(){
        checkInBttn = UIButton()
        self.addSubview(checkInBttn!)
        checkInBttn?.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(self.snp.bottom).inset(10)
            make.right.equalTo(self.snp.right).inset(10)
            make.height.equalTo(25)
            make.width.equalTo(self.snp.width).dividedBy(3)
        }
        checkInBttn?.setTitle("Go", for: .normal)
        checkInBttn?.setTitle("Going", for: .selected)
        checkInBttn?.layer.borderColor = kThemeBrighterColor.cgColor
        //checkInBttn?.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        checkInBttn?.titleLabel?.font = UIFont.preferredFont(forTextStyle: .headline)
        checkInBttn?.setTitleColor(kThemeMainColor, for: .normal)
        checkInBttn?.setTitleColor(UIColor.white, for: .selected)
        checkInBttn?.setBackgroundColor(color: kThemeBrighterColor, forState: .selected)
        checkInBttn?.setBackgroundColor(color: UIColor.white, forState: .normal)
        checkInBttn?.isUserInteractionEnabled = true
        checkInBttn?.layer.cornerRadius = 3.0
        checkInBttn?.layer.masksToBounds = true
        checkInBttn?.layer.borderWidth = 1.0
        checkInBttn?.addTarget(self, action: #selector(goButtonTapped(_sender:)), for: .touchUpInside)
    }
    
    func setInviteLabel(text: String){
        /*
        attendeesCollView = UIView()
        self.addSubview(attendeesCollView!)
        attendeesCollView?.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo((checkInBttn?.snp.bottom)!)
            make.left.equalTo(self.snp.left).offset(10)
            make.height.equalTo(25)
            make.width.equalTo(self.snp.width).multipliedBy(5.0/9.0)
        }
        attendeesCollView?.backgroundColor = kThemeBrighterColor
        attendeesCollView?.layer.cornerRadius = 3.0
         */
        invitedByLabel = UILabel()
        self.addSubview(invitedByLabel!)
        invitedByLabel?.snp.makeConstraints{ (make) -> Void in
            //make.bottom.equalTo((self.attendeesCollView?.snp.top)!).offset(5)
            make.top.lessThanOrEqualTo((self.locationLabel?.snp.bottom)!).offset(5)
            make.left.lessThanOrEqualTo((self.locationLabel?.snp.left)!)
            //make.centerX.equalTo(self.snp.centerX)
            make.height.lessThanOrEqualTo(15)
            make.width.lessThanOrEqualTo(self.snp.width).multipliedBy(0.90)
        }
        //invitedByLabel?.frame.origin.y = (checkInBttn?.frame.origin.y)! - (self.frame.height) * 2/40
        invitedByLabel?.text = "Invited by @\(text)"
        //invitedByLabel?.font = UIFont.boldSystemFont(ofSize: 10)
        invitedByLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        //invitedByLabel?.textColor = UIColor.lightGray
    }
    
    
    func setupView(titleText: String,timeText: String, addressText: String, picURL: URL, peopleGoing: [ContactViewModel]){
        /*
        expandButton = UIButton()
        expandButton?.frame = self.layer.frame
        expandButton?.backgroundColor = UIColor.clear
        
        self.addSubview(expandButton!)*/
        setThumbnailView(image: picURL)
        setTitle(text: titleText)
        setTimeLabel(time: timeText)
        setLocationLabel(location: addressText)
        setGoButton()
        setInviteLabel(text: "hmoneygotit")
        setExpandButton()
    }
    
    func setExpandButton(){
        expandButton = UIButton()
        self.addSubview(self.expandButton!)
        expandButton?.addTarget(self, action: #selector(expandButtonTapped), for: .touchUpInside)
        self.expandButton?.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo((self.snp.width)).multipliedBy(1)
            make.height.equalTo((self.snp.height)).multipliedBy(0.1)
            make.centerX.equalTo((self.snp.centerX))
            make.top.greaterThanOrEqualTo((self.invitedByLabel?.snp.bottom)!)
            make.bottom.lessThanOrEqualTo((self.checkInBttn?.snp.top)!)
        }
        self.expandButton?.setTitleColor(UIColor.black, for: .normal)
        self.expandButton?.setTitleColor(kThemeMainColor, for: .normal)
        self.expandButton?.setTitle("More Details...", for: .normal)
        self.expandButton?.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        self.expandButton?.layer.borderColor = UIColor.flatRed().cgColor
        
    }
    
    func goButtonTapped(_sender:UIButton)
    {
        _sender.isSelected = !_sender.isSelected;
    }
    
    func moreButtonTapped(_sender:UIButton){
        log.info("More button tapped")
        delegate?.openActionSheet()
    }
    
    func playButtonTapped(_sender:UIButton){
        log.info("Play button tapped")
        delegate?.playVideo()
    }
    
    func expandButtonTapped(){
        log.info("Expand Button tapped")
        delegate?.expandView()
    }
    
    convenience init(_viewModel: MouveCardViewModel, frame: CGRect) {
        self.init(frame: frame)
        self.viewModel = _viewModel
        self.setupView(titleText: viewModel.title, timeText: viewModel.time, addressText: viewModel.location, picURL:viewModel.thumbnail ,peopleGoing: viewModel.peopleGoing)
    }
    convenience init(_viewModel: MouveCardViewModel) {
        self.init()
        self.viewModel = _viewModel
        self.setupView(titleText: viewModel.title, timeText: viewModel.time, addressText: viewModel.location, picURL:viewModel.thumbnail ,peopleGoing: viewModel.peopleGoing)
    }

    @IBAction func checkInMouve(_ sender: AnyObject) {
        viewModel.toggleCheckIn()
    }
}

protocol MouveCardViewDelegate{
    func openActionSheet()
    func playVideo()
    func expandView()
}
