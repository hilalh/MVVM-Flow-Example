//
//  MouveCardViewModel.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/20/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import Nuke

import RxSwift
import RxCocoa

class MouveCardViewModel {
    
    var mouve: abstractMouve
    var title: String{
        return mouve.name
    }
    var time: String{
        return buildTimeRange()
    }
    
    var location: String{
        return mouve.address
    }
    var thumbnail: URL{
        return URL(string: mouve.thumbnailImgURL!)!
    }
    var video: URL{
        return URL(string: mouve.videoURL!)!
    }
    var peopleGoing: [ContactViewModel]{
        return fetchUsersGoing()
    }
    

    init(mouve: abstractMouve) {
        self.mouve = mouve
    }
    
    private func buildTimeRange() -> String{
        return "\(mouve.startTime) - \(mouve.endTime)"
    }
    
    private func fetchUsersGoing() -> [ContactViewModel]{
        return dataManager.fakeData.attendents.map{ContactViewModel(user: $0)}
        /*
        var users = [ContactViewModel]()
        for attendent in mouve.attending{
            users.append(ContactViewModel(user: attendent))
        }
        return users
         */
    }
    
    
    func reportMouve(){
        log.info("Reported Mouve")
    }
    
    private class func fetchVideo(mouve: abstractMouve) -> NSData{
        return NSData()
    }
    
    private class func fetchPeopleGoing(mouve: abstractMouve) -> String{
        let goingString: String? = ""
        return goingString!
    }
    
    func toggleCheckIn() {
        dataManager.shared.toggleAttendMouve(self.mouve)
    }


}
