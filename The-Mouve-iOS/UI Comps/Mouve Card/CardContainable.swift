//
//  CardContainable.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/11/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation

protocol CardContainable{
    func setupInCard()
    func displayContent()
    func destroyContent()
}