//
//  ContainableVideoPlayer.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/12/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

class ContainableVideoPlayer: UIView, CardContainable {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    func setupInCard() {
        print("Setting up containable in card...")
    }
    
    func displayContent() {
        print("Displaying content...")
    }
    
    func destroyContent() {
        print("Destroying content...")
    }


}
