//
//  ContainableMap.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/12/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

class ContainableMap: UIView, CardContainable {
    func setupInCard() {
        print("Setting up containable in card...")
    }
    
    func displayContent() {
        print("Displaying content...")
    }
    
    func destroyContent() {
        print("Destroying content...")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
