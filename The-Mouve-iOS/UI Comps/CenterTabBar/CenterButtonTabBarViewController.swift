//
//  CenterButtonTabBarViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/9/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

class CenterButtonTabBarViewController: UITabBarController {

    var centerImage: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTabBarColors()
        centerImage = prepareCenterTabBarButton(centerImage: #imageLiteral(resourceName: "add-mouve-icon"))
        self.view.addSubview(centerImage!)
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centerButtonTapped(){
        print("Center Button Tapped")
        let vc = CreateMouveViewController()
        let nav = UINavigationController(rootViewController: vc)
        self.show(nav, sender: self)
        
//        UIApplication.shared.keyWindow?.rootViewController = nav
//        nav.pushViewController(vc, animated: true)
        
    }
    
    func setTabBarColors(){
        self.tabBar.tintColor = UIColor.flatMint()
        self.tabBar.isTranslucent = false
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.shadowImage = UIImage()
        
    }
    
    
    func prepareCenterTabBarButton(centerImage: UIImage) -> UIImageView{
        let buttonImage = centerImage
//        let button = UIButton(type: .custom)
        let button = UIImageView()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 64.0, height: 64.0)
        button.image = buttonImage

        
        button.layer.cornerRadius = button.bounds.height / 2.0
        button.clipsToBounds = true
        
        let heightDifference: CGFloat = buttonImage.size.height - self.tabBar.frame.height / 2
        if (heightDifference < 0){
            button.center = self.tabBar.center
        }
        else
        {
            var center: CGPoint = self.tabBar.center;
            center.y = center.y - heightDifference/1.5;
            button.center = center;
        }

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(centerButtonTapped))
        button.isUserInteractionEnabled = true
        button.addGestureRecognizer(tapGestureRecognizer)
        return button
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
