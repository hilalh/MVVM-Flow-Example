//
//  ContactsTableViewCell.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/12/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke
import SnapKit

class ContactsTableViewCell: UITableViewCell {
    var profileImageView: RoundableUIImageView?
    var fullNameLabel: UILabel?
    var usernameLabel: UILabel?
    var active: Bool = false
    
    func setupView(viewModel: ContactViewModel){
        try! self.setupView(fullName: viewModel.fullName.value(), username: viewModel.username.value(), image: viewModel.picURL.value(), active: viewModel.active.value())
    }
    func setupView(fullName: String, username: String, image: URL, active: Bool){
        //Let's us have the cell not selected
        self.active = active
        self.selectionStyle = .none
        profileImageView = setProfileImage(image: image)
        self.addSubview(profileImageView!)
        profileImageView?.snp.makeConstraints{(make) -> Void in
            make.centerY.equalTo(self.snp.centerY)
            make.left.equalTo(self.snp.left).offset(5)
            make.height.equalTo(self.snp.height).multipliedBy(0.8)
            make.height.lessThanOrEqualTo(48)
            make.width.equalTo(self.snp.height).multipliedBy(0.8)
            make.width.lessThanOrEqualTo(48)
        }
        profileImageView?.contentMode = .scaleAspectFill
        
        
        usernameLabel = setUsername(text: username)
        self.addSubview(usernameLabel!)
        usernameLabel?.snp.makeConstraints{(make) -> Void in
            make.bottom.equalTo((profileImageView?.snp.centerY)!)
            make.left.equalTo((profileImageView?.snp.right)!).offset(15)
            make.height.equalTo(self.snp.height).multipliedBy(0.35)
            make.width.equalTo(self.snp.width).multipliedBy(0.6)
        }
        usernameLabel?.numberOfLines = 0
        fullNameLabel = setFullName(text: fullName)
        self.addSubview(fullNameLabel!)
        fullNameLabel?.snp.makeConstraints{(make) -> Void in
            make.top.equalTo((profileImageView?.snp.centerY)!)
            make.left.equalTo((usernameLabel?.snp.left)!)
            make.height.equalTo(self.snp.height).multipliedBy(0.35)
            make.width.equalTo(self.snp.width).multipliedBy(0.6)
        }
        fullNameLabel?.numberOfLines = 0
        
        
    }
    convenience init(viewModel: ContactViewModel) {
        self.init()
        self.setupView(viewModel: viewModel)
    }
    func toggleActive(){
        if(active){
            self.profileImageView?.borderWidth = 1.5
        }
        else{
            self.profileImageView?.borderWidth = 0
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView?.round = true
        toggleActive()
    }
    
    func setFullName(text: String) -> UILabel{
        let label = UILabel()
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = UIColor.black
        return label
    }
    
    func setUsername(text: String) -> UILabel{
        let label = UILabel()
        label.text = "@\(text)"
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        label.textColor = UIColor.lightGray
        return label
    }
    
    func setProfileImage(image: URL) -> RoundableUIImageView{
        let imageView = RoundableUIImageView()
        Nuke.loadImage(with: image, into: imageView)
        //let imageView = UIImageView(image: image)
        imageView.borderColor = kThemeBrighterColor
        
        //imageView.round = true
        return imageView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
//Add Button extension
extension ContactsTableViewCell{
    static func addCell(viewModel: ContactViewModel) -> ContactsTableViewCell{
        let cell = ContactsTableViewCell(viewModel: viewModel)
        cell.addCell(viewModel: viewModel)
        return cell
    }
    func addCell(viewModel: ContactViewModel){
        self.setupView(viewModel: viewModel)
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 25))
        button.setTitle("Add", for: .normal)
        button.setTitleColor(kThemeMainColor, for: .normal)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.layer.borderColor = kThemeMainColor.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 10
        self.accessoryView = button
    }
}

//Invite Button extension
extension ContactsTableViewCell{
    static func inviteCell(viewModel: ContactViewModel) -> ContactsTableViewCell{
        let cell = ContactsTableViewCell(viewModel: viewModel)
        cell.inviteCell(viewModel: viewModel)
        return cell
    }
    func inviteCell(viewModel: ContactViewModel){
        self.setupView(viewModel: viewModel)
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 25))
        button.setTitle("Invite", for: .normal)
        button.setTitle("Invited", for: .selected)
        button.setTitleColor(kThemeMainColor, for: .normal)
        button.setTitleColor(UIColor.white, for: .selected)
        
        button.setBackgroundColor(color: UIColor.white, forState: .normal)
        button.setBackgroundColor(color: kThemeMainColor, forState: .selected)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.layer.borderColor = kThemeMainColor.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(accesorryButtonTapped(_sender:)), for: .touchUpInside)
        self.accessoryView = button
    }
    func accesorryButtonTapped(_sender:UIButton)
    {
        _sender.isSelected = !_sender.isSelected;
    }
}
