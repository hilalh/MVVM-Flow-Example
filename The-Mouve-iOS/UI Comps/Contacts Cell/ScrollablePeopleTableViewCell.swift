//
//  ScrollablePeopleTableViewCell.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/6/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke
import RxSwift
import RxCocoa

class ScrollablePeopleTableViewCell: UITableViewCell {
    var collectionView: UICollectionView?
    var headerLabel: UILabel = UILabel()
    var collectionViewOffset: CGFloat {
        get {
            return (collectionView?.contentOffset.x)!
        }
        
        set {
            collectionView?.contentOffset.x = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCollectionView(){
        //Let's us have the cell not selected
        self.selectionStyle = .none
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        self.addSubview(collectionView!)
        collectionView?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.height.equalTo(self.snp.height).multipliedBy(0.7)
            //make.width.equalTo(self.snp.width).multipliedBy(0.95)
            //make.width.equalTo(self.snp.width)
            make.bottom.equalTo(self.snp.bottom)
        }
        headerLabel.text = "Latest Pings"
        headerLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        self.addSubview(headerLabel)
        headerLabel.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo((collectionView?.snp.width)!)
            make.height.equalTo(self.snp.height).multipliedBy(0.2)
            make.bottom.greaterThanOrEqualTo((collectionView?.snp.top)!).offset(1)
            make.bottom.lessThanOrEqualTo((collectionView?.snp.top)!).offset(2)
        }
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int, cellIdentifier: String) {
        
        collectionView?.delegate = dataSourceDelegate
        collectionView?.dataSource = dataSourceDelegate
        collectionView?.register(PeopleCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collectionView?.tag = row
        collectionView?.reloadData()
    }

}

extension ScrollablePeopleTableViewCell{
    func setPingsColl(){
        self.collectionView?.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(self.snp.width).multipliedBy(0.95)
            make.centerX.equalTo(self.snp.centerX)
        }
    }
}

class PeopleCollectionViewCell: UICollectionViewCell{
    var profilePic: RoundableUIImageView?
    var nameLabel: UILabel?
    func setCell(ping: PingViewModel){
        setCell(viewModel: ContactViewModel(user: ping.fetchCreator()))
    }
    func setCell(viewModel: ContactViewModel){
        try! setCell(name: viewModel.fullName.value(), pic: viewModel.picURL.value())
    }
    func setCell(name: String, pic: URL){
        profilePic = RoundableUIImageView()
        self.addSubview(profilePic!)
        profilePic?.snp.makeConstraints{(make) -> Void in
            make.height.equalTo(self.snp.height).multipliedBy(0.7)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(self.snp.height).multipliedBy(0.7)
        }
        Nuke.loadImage(with: pic, into: profilePic!)
        
        nameLabel = UILabel()
        self.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints{(make) -> Void in
            make.height.equalTo(self.snp.height).multipliedBy(0.2)
            make.centerX.equalTo(self.snp.centerX)
            make.width.equalTo(self.snp.width)
            make.top.equalTo((profilePic?.snp.bottom)!)
        }
        nameLabel?.text = name
        nameLabel?.textAlignment = .center
        nameLabel?.adjustsFontSizeToFitWidth = true
        nameLabel?.numberOfLines = 0
        //nameLabel?.lineBreakMode = .byWordWrapping
        nameLabel?.font = UIFont.preferredFont(forTextStyle: .caption2)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profilePic?.round = true
    }

}
