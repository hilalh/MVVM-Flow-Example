//
//  ProfileScrollView.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/27/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke


class ProfileScrollView: UIView, UIScrollViewDelegate {
    
    var scrollView: UIScrollView?
    var tableView: UITableView?
    var profileImageView: UIImageView?
    var headerImageView: UIImageView?
    var lastContacts: UIView?
    var header: UIView?
    var headerLabel: UILabel?
    var subtitleLabel: UILabel?
    var titleLabel: UILabel?
    var headerButton: UIButton?
    var delegate: ProfileScrollViewDelegate?
    var leftButtonView: UIImageView?
    var rightButtonView: UIImageView?
    let offset_HeaderStop:CGFloat = 40 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 95 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 35 // The distance between the bottom of the Header and the top of the White Label
    var headerHeight: CGFloat {
        return (1/6)*self.frame.height
    }
    //let headerHeight: CGFloat = 118
    var profileImageY:CGFloat {
        return (headerHeight / 2)
    }
    var profileImageDiam: CGFloat {
        return (1/6)*self.frame.height
    }
    //let profileImageDiam: CGFloat = 148
    var titleY: CGFloat {
        return (profileImageY + profileImageDiam)
    }
    var subtitleY: CGFloat{
        return (titleY + (headerHeight / 5))
    }
    
    
    static func initScrollView(bgColor: UIColor, picURL: URL, title: String, subtitle: String,buttonTitle: String, height: CGFloat) -> ProfileScrollView{
        let bounds = UIScreen.main.bounds
        let view = ProfileScrollView(frame: bounds)
        view.setupScroll(bgColor: bgColor, picURL: picURL, title: title, subtitle: subtitle, buttonTitle: buttonTitle, height: height)
        return view
    }
    
    static func initTableView(bgColor: UIColor, picURL: URL, title: String, subtitle: String,buttonTitle: String) -> ProfileScrollView{
        
        let bounds = UIScreen.main.bounds
        let view = ProfileScrollView(frame: bounds)
        view.setupTable(bgColor: bgColor, picURL: picURL, title: title, subtitle: subtitle, buttonTitle: buttonTitle)
        view.tableView?.addObserver(view, forKeyPath: "contentOffset", options: .new, context: nil)
        return view
    }


    func setupHeaderLabel(title: String) -> UILabel{
//        let label = UILabel(frame: CGRect(x: (self.center.x), y: (self.header?.frame.size.height)! - 5, width: self.frame.width, height: 25))
        
        let label = UILabel(frame: CGRect(x: (self.center.x), y: headerHeight , width: self.frame.width, height: 25))
        label.center.x = self.center.x
        label.textAlignment = .center
        label.text = title
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 18)
        label.textColor = UIColor.white
        
        return label
    }
    
    func setupProfileImage(image: URL) -> UIImageView{
        let imageView = UIImageView(frame: CGRect(x: (self.center.x), y: profileImageY, width: profileImageDiam, height: profileImageDiam))

        Nuke.loadImage(with: image, into: imageView)
        imageView.center.x = self.center.x
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = imageView.bounds.width / 2
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.flatMint().darken(byPercentage: 0.3).cgColor
        imageView.clipsToBounds = true
        
        return imageView
    }
    
    func setupTitle(text: String) -> UILabel{
        let label = UILabel(frame: CGRect(x: (self.center.x), y: titleY, width: headerHeight*2, height: headerHeight/4))
        label.center.x = self.center.x
        label.textAlignment = .center
        label.text = text
        
        return label
    }
    
    func setupSubtitle(text: String) -> UILabel{
        let label = UILabel(frame: CGRect(x: (self.center.x), y: subtitleY, width: headerHeight*2, height: headerHeight/4))
        label.center.x = self.center.x
        label.textAlignment = .center
        label.text = text
        label.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        label.textColor = UIColor.lightGray
        
        return label
    }
    
    
    func setupHeaderImageView(color: UIColor) -> UIImageView{
        let imageView = UIImageView(frame: (self.header?.frame)!)
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = color
        return imageView
    }
    
    func setupHeaderButton(title: String) -> UIButton{
        let button = UIButton(frame: CGRect(x: self.frame.size.width - 100, y: 120, width: 80, height: 35))
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .normal)
        button.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 12)
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(scrollButtonClicked), for: .touchUpInside)
        
        return button
    }
    
    func setupTable(bgColor: UIColor, picURL: URL, title: String, subtitle: String,buttonTitle: String){
//        Setup Header
        self.header = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: headerHeight))
        self.addSubview(self.header!)
        self.headerLabel = setupHeaderLabel(title: title)
        self.header?.addSubview(self.headerLabel!)

//        Setup Table
        self.tableView = UITableView(frame: self.frame)
        self.tableView?.backgroundColor = UIColor.clear
        self.tableView?.center.x = self.center.x
        self.tableView?.showsVerticalScrollIndicator = false
        self.tableView?.tableHeaderView = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: (self.header?.frame.height)! + (1.0)*headerHeight))
        self.addSubview(self.tableView!)
        
//        Setup Rest
        self.titleLabel = setupTitle(text: title)
//        self.subtitleLabel = setupSubtitle(text: subtitle)
        self.profileImageView = setupProfileImage(image: picURL)
        if(buttonTitle.characters.count > 0){
            self.headerButton = setupHeaderButton(title: buttonTitle)
        }
        self.tableView?.addSubview(profileImageView!)
        self.tableView?.addSubview(titleLabel!)
//        self.tableView?.addSubview(subtitleLabel!)
        if(buttonTitle.characters.count > 0){
            self.tableView?.addSubview(headerButton!)
        }
        
        self.headerImageView = setupHeaderImageView(color: bgColor)
        
        self.header?.insertSubview(self.headerImageView!, aboveSubview: self.headerLabel!)
        self.header?.clipsToBounds = true
        
    }

    func setupScroll(bgColor: UIColor, picURL: URL, title: String, subtitle: String,buttonTitle: String,height: CGFloat){
//        Setup Header
        self.header = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: headerHeight*2))
        self.addSubview(self.header!)
        self.headerLabel = setupHeaderLabel(title: title)
        self.header?.addSubview(self.headerLabel!)
        
//        Setup Scroll
        self.scrollView = UIScrollView(frame: self.frame)
        self.scrollView?.showsVerticalScrollIndicator = false
        self.addSubview(self.scrollView!)
        
        let newSize: CGSize = CGSize(width: self.frame.size.width, height: height)
        self.scrollView?.contentSize = newSize
        self.scrollView?.delegate = self
        
//        Setup Rest
        self.titleLabel = setupTitle(text: title)
        self.subtitleLabel = setupSubtitle(text: subtitle)
        self.profileImageView = setupProfileImage(image: picURL)
        if(buttonTitle.characters.count > 0){
            self.headerButton = setupHeaderButton(title: buttonTitle)
        }
//        Add to Scroll
        self.scrollView?.addSubview(profileImageView!)
        self.scrollView?.addSubview(titleLabel!)
        self.scrollView?.addSubview(subtitleLabel!)
        if(buttonTitle.characters.count > 0){
            self.scrollView?.addSubview(headerButton!)
        }
        self.headerImageView = setupHeaderImageView(color: bgColor)
        self.header?.insertSubview(self.headerImageView!, aboveSubview: self.headerLabel!)
        self.header?.clipsToBounds = true
        

    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        self.animationForScroll(offset: offset)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let offset = tableView?.contentOffset.y
        self.animationForScroll(offset: offset!)
    }
    
    func animationForScroll(offset: CGFloat){
        var headerTransform: CATransform3D = CATransform3DIdentity
        var profileTransform: CATransform3D = CATransform3DIdentity
      // DOWN -----------------
        if(offset < 0){
            let headerScaleFactor = (-(offset) / (self.header?.bounds.size.height)!)
            let headerSizevariation = (((self.header?.bounds.size.height)! * (1.0 + headerScaleFactor)) - (self.header?.bounds.size.height)!)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0 ,headerSizevariation , 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            self.header?.layer.transform = headerTransform
            if(offset < -self.frame.size.height / 3.5){
                self.receivedScrollEvent()
            }
            
        }
        //UP
        else{
            // Header -----------
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0);
            
            //  ------------ Label
                let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0);
                self.headerLabel?.layer.transform = labelTransform;
                self.headerLabel?.layer.zPosition = 2;
            //    // Avatar -----------
            let avatarScaleFactor: CGFloat = (min(offset_HeaderStop, offset)) / (self.profileImageView?.bounds.size.height)! / 1.4;
            // Slow down the animation
            let avatarSizeVariation: CGFloat = (((self.profileImageView?.bounds.size.height)! * (1.0 + avatarScaleFactor)) - (self.profileImageView?.bounds.size.height)!) / 2.0;
            profileTransform = CATransform3DTranslate(profileTransform, 0, avatarSizeVariation, 0);
            profileTransform = CATransform3DScale(profileTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0);

            
            if (offset <= offset_HeaderStop) {
                if ((self.profileImageView?.layer.zPosition)! <= (self.headerImageView?.layer.zPosition)!) {
                    self.header?.layer.zPosition = 0;
                }
            }else {
                if ((self.profileImageView?.layer.zPosition)! >= (self.headerImageView?.layer.zPosition)!) {
                    self.header?.layer.zPosition = 2;
                }
            }
        }
        self.header?.layer.transform = headerTransform
        self.profileImageView?.layer.transform = profileTransform

    }

    





    func scrollButtonClicked(){
        self.delegate?.scrollButtonClicked()
    }

    func receivedScrollEvent(){
        self.delegate?.receivedScrollEvent()
    }
    
    func leftButtonClicked(){
        print("Left Button Clicked")
        self.delegate?.leftButtonClicked()
    }
    func rightButtonClicked(){
        print("Right Button Clicked")
        self.delegate?.rightButtonClicked()
    }

    func setButtons(rightImage: UIImage? = nil, leftImage: UIImage? = nil){
        if(rightImage != nil){
            rightButtonView = UIImageView(image: rightImage?.withRenderingMode(.alwaysTemplate))
            
            rightButtonView?.frame = CGRect(x: ((self.frame.width - 5) - 32), y: UIApplication.shared.statusBarFrame.height + 5, width: 28, height: 28)
            rightButtonView?.clipsToBounds = true
            rightButtonView?.tintColor = UIColor.white
            rightButtonView?.contentMode = .scaleAspectFit
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(rightButtonClicked))
            rightButtonView?.isUserInteractionEnabled = true
            rightButtonView?.addGestureRecognizer(tapGestureRecognizer)
            if(tableView != nil){
                tableView?.addSubview(rightButtonView!)
            }
            else if(scrollView != nil){
                scrollView?.addSubview(rightButtonView!)
            }
            else{
                print("None were initialized")
            }

        }
        if(leftImage != nil){
            leftButtonView = UIImageView(image: leftImage?.withRenderingMode(.alwaysTemplate))
            leftButtonView?.frame = CGRect(x: 9, y: UIApplication.shared.statusBarFrame.height + 5, width: 32, height: 32)
            leftButtonView?.clipsToBounds = true
            leftButtonView?.contentMode = .scaleAspectFit
            leftButtonView?.tintColor = UIColor.white
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(leftButtonClicked))
            leftButtonView?.isUserInteractionEnabled = true
            leftButtonView?.addGestureRecognizer(tapGestureRecognizer)
            if(tableView != nil){
                tableView?.addSubview(leftButtonView!)
            }
            else if(scrollView != nil){
                scrollView?.addSubview(leftButtonView!)
            }
            else{
                print("None were initialized")
            }
        }
    }
}

protocol ProfileScrollViewDelegate{
    func scrollButtonClicked()
    func receivedScrollEvent()
    func leftButtonClicked()
    func rightButtonClicked()
}
