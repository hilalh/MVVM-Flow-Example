//
//  ProfilePopupView.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/7/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke
import XLActionController

class ProfilePopupView: UIViewController {
    var viewModel: ContactViewModel?
    var profileImageView = RoundableUIImageView()
    var nameLabel = UILabel()
    var usernameLabel = UILabel()
    var pingButton = UIButton()
    var container = UIView()
    
    convenience init(_viewModel: ContactViewModel) {
        self.init()
        viewModel = _viewModel
        try! setMainView(picURL: (viewModel?.picURL.value())!, name: (viewModel?.fullName.value())!, username: (viewModel?.username.value())!)
    }
    
    func setMainView(picURL: URL, name: String, username: String){
            
        self.view.addSubview(container)
        container.snp.makeConstraints{ (make) -> Void in
            make.height.equalTo(self.view.snp.height).multipliedBy(0.20)
            make.width.equalTo(self.view.snp.width).multipliedBy(0.792)
            make.center.equalTo(self.view.snp.center)
        }
        container.backgroundColor = UIColor.white
        container.layer.cornerRadius = 4
        Nuke.loadImage(with: picURL, into: profileImageView)
        view.addSubview(profileImageView)
        profileImageView.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width).multipliedBy(0.4)
            make.height.equalTo(container.snp.width).multipliedBy(0.4)
            make.centerX.equalTo(container.snp.centerX)
            make.centerY.equalTo(container.snp.top)
        }
        profileImageView.borderWidth = 4
        profileImageView.borderColor = UIColor.white
        
        let moreBtnView = UIButton()
        container.addSubview(moreBtnView)
        moreBtnView.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(20)
            make.height.equalTo(4)
            make.top.equalTo(container.snp.top).offset(5)
            make.right.equalTo(container.snp.right).inset(5)
        }
        moreBtnView.setImage(#imageLiteral(resourceName: "xMore"), for: .normal)
        moreBtnView.tintColor = UIColor.darkGray
        moreBtnView.addTarget(self, action: #selector(openActionSheet), for: .touchUpInside)
        container.addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width)
            make.height.equalTo(container.snp.height).multipliedBy(0.15)
            make.top.equalTo(profileImageView.snp.bottom).offset(3)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        usernameLabel.text = "@\(username)"
        usernameLabel.textAlignment = .center
        usernameLabel.adjustsFontSizeToFitWidth = true
        usernameLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)

        container.addSubview(nameLabel)
        nameLabel.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width)
            make.height.equalTo(container.snp.height).multipliedBy(0.15)
            make.top.equalTo(usernameLabel.snp.bottom).offset(3)
            make.centerX.equalTo(container.snp.centerX)
        }
        nameLabel.text = name
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        self.view.addSubview(pingButton)
        pingButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width)
            make.height.equalTo(container.snp.height).dividedBy(4)
            make.top.equalTo(container.snp.bottom).offset(20)
            make.centerX.equalTo(container.snp.centerX)
        }
        pingButton.setTitle("PING", for: .normal)
        pingButton.backgroundColor = UIColor.flatRed()
        pingButton.layer.cornerRadius = 4
        pingButton.titleLabel?.textAlignment = .center
        pingButton.titleLabel?.textColor = UIColor.white
        pingButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        pingButton.addTarget(self, action: #selector(pingTapped), for: .touchUpInside)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let exitButton = UIButton()
        self.view.addSubview(exitButton)
        exitButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(self.view.snp.width).multipliedBy(0.1)
            make.height.equalTo(self.view.snp.width).multipliedBy(0.1)
            make.bottom.equalTo(self.view.snp.bottom).inset(10)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        exitButton.setImage(#imageLiteral(resourceName: "slickX").withRenderingMode(.alwaysTemplate), for: .normal)
        exitButton.tintColor = UIColor.flatMint().lighten(byPercentage: 0.3)
        exitButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.round = true
    }
    
    func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func pingTapped(){
        print("Ping Tapped")
    }
    
    func openActionSheet(){
        let actionController = MouveActionController()
        actionController.headerData = "What would you like to do?"
        actionController.addAction(Action("Report this user", style: .destructive, handler: { action in
            //self.mouveViewModel?.reportMouve()
            print("Report: \(self.viewModel)")
        }))
         actionController.addAction(Action("Block this user", style: .cancel, handler: { action in
                print("Blocked user")
         }))
        actionController.addAction(Action("Cancel", style: .default, handler:nil))
        
        self.present(actionController, animated: true, completion: nil)
        
    }
    
}
