//
//  UIButton+RoundableImage.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/12/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

@IBDesignable class RoundableUIButton: UIButton {
    fileprivate var _round = false
    fileprivate var _borderColorOn = UIColor.black
    fileprivate var _borderColorOff = UIColor.black
    @IBInspectable var borderWidth: CGFloat {
        set {
            self.layer.borderWidth = newValue
        }
        get {
            return self.layer.borderWidth
        }
    }
    @IBInspectable var borderColorForOnState: UIColor {
        set {
            _borderColorOn = newValue
            self.layer.borderColor = _borderColorOn.cgColor
        }
        get {
            return self._borderColorOn
        }
    }
    @IBInspectable var borderColorForOffState: UIColor {
        set {
            _borderColorOff = newValue
            self.layer.borderColor = _borderColorOff.cgColor
        }
        get {
            return self._borderColorOff
        }
    }

    @IBInspectable var round: Bool {
        set {
            _round = newValue
            makeRound()
        }
        get {
            return self._round
        }
    }

    override var isEnabled: Bool {
        set {
            super.isEnabled = newValue
            newValue == true ? (self.layer.borderColor = _borderColorOn.cgColor) : (self.layer.borderColor = _borderColorOff.cgColor)
        }
        get {
            return super.isEnabled
        }
    }
    override internal var frame: CGRect {
        set {
            super.frame = newValue
            makeRound()
        }
        get {
            return super.frame
        }

    }

    fileprivate func makeRound() {
        if self.round == true {
            self.clipsToBounds = true
            self.imageView?.clipsToBounds = true
            self.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            self.layer.cornerRadius = (self.frame.width + self.frame.height) / 4
        } else {
            self.layer.cornerRadius = 0
        }
    }

}
