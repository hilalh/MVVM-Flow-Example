//
//  UILabel+Timer.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/18/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import UIKit

class TimerLabel: UILabel{
    private var countDownTimer: Timer = Timer()
    private var timerValue: Int = 10
    
    convenience init(color: UIColor){
        self.init()
        self.textColor = color
        self.textAlignment = .center
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 2
        self.layer.borderColor = color.cgColor
        self.adjustsFontSizeToFitWidth = true
    }
    
    private func updateLabel(value: Int) {
        self.setLabelText(value: self.timeFormatted(totalSeconds: value))
    }
    
    private func setLabelText(value: String) {
        self.text = value
    }
    
    private func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        //        let minutes: Int = (totalSeconds / 60) % 60
        //        let hours: Int = totalSeconds / 3600
        return String(format: "%02d", seconds)
    }
    
    // Needs @objc to be able to call private function in NSTimer.
    @objc private func countdown(dt: Timer) {
        self.timerValue -= 1
        if self.timerValue < 0 {
            self.countDownTimer.invalidate()
        }
        else {
            self.setLabelText(value: self.timeFormatted(totalSeconds: self.timerValue))
        }
    }
    
    func setTimer(value: Int) {
        self.timerValue = value
        self.updateLabel(value: self.timerValue)
        self.countDownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countdown(dt:)), userInfo: nil, repeats: true)
        
    }
}
