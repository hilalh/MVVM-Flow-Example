//
//  RoundableUIImageView.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/11/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

@IBDesignable class RoundableUIImageView: UIImageView {
    fileprivate var _round = false
    fileprivate var _borderColor = UIColor.black
    @IBInspectable var borderWidth: CGFloat{
        set {
            self.layer.borderWidth = newValue
        }
        get {
            return self.layer.borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor{
        set {
            _borderColor = newValue
            self.layer.borderColor = _borderColor.cgColor
        }
        get {
            return self._borderColor
        }
    }
    
    @IBInspectable var round: Bool {
        set {
            _round = newValue
            makeRound()
        }
        get {
            return self._round
        }
    }
    
    override internal var frame: CGRect {
        set {
            super.frame = newValue
            makeRound()
        }
        get {
            return super.frame
        }
        
    }
    
    
    fileprivate func makeRound() {
        if self.round == true {
            self.clipsToBounds = true
            self.layer.cornerRadius = (self.frame.width + self.frame.height) / 4

        } else {
            self.layer.cornerRadius = 0
        }
    }
}
