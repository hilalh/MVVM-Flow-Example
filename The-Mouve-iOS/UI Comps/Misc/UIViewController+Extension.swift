//
//  UIViewController+Extension.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/14/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

extension UIViewController  {
    func setNavBar(title: String, leftButton: UIBarButtonItem? = nil, rightButton: UIBarButtonItem? = nil){
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = kThemeMainColor
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.setLeftBarButton(leftButton, animated: true)
        navigationItem.setRightBarButton(rightButton, animated: true)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        navigationItem.title = title
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 20.0)]
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
