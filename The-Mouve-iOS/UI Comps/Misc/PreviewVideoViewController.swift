//
//  PreviewVideoViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/5/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AVFoundation

class PreviewVideoViewController: UIViewController {

    var videoUrl: URL?
    var timeObserver: Any!
    weak var avPlayer = AVPlayer()
    weak var avPlayerLayer = AVPlayerLayer()
    let exitButton = UIButton()
    let timeRemainingLabel = UILabel()

    convenience init(videoUrl: URL) {
        self.init()
        self.videoUrl = videoUrl
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.avPlayer?.play()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black
        let item = AVPlayerItem(url: self.videoUrl!)
        self.avPlayer = AVPlayer(playerItem: item)
        self.avPlayer?.actionAtItemEnd = .none
        //NotificationCenter.default.rx.notification(NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer.currentItem).map{observer in
          //  self.animationDidFinish(observer)
        //}
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.animationDidFinish(_:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: self.avPlayer?.currentItem)
        
        setPlayerLayer()
        self.view.layer.addSublayer(self.avPlayerLayer!)
        let timeInterval: CMTime = CMTimeMakeWithSeconds(1.0, 10)
        timeObserver = avPlayer?.addPeriodicTimeObserver(forInterval: timeInterval, queue:DispatchQueue.main){ (elapsedTime: CMTime) -> Void in
                    //log.info("elapsedTime now:", CMTimeGetSeconds(elapsedTime))
            self.observeTime(elapsedTime: elapsedTime)
        }
        timeRemainingLabel.textColor = kThemeBrighterColor
        timeRemainingLabel.textAlignment = .center
        
        view.addSubview(timeRemainingLabel)
        timeRemainingLabel.snp.makeConstraints{ (make) -> Void in
            make.centerX.equalTo(self.view.snp.centerX)
            make.top.equalTo(self.view.snp.top)
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.view.snp.height).dividedBy(8)
        }
        setExitButton()
        self.view.addSubview(self.exitButton)
    }
    func animationDidFinish(_ notification: Notification) {
        log.info("Animation did finish")
        self.dismiss(animated: true, completion: nil)
    }
    private func updateTimeLabel(elapsedTime: Float64, duration: Float64) {
        let timeRemaining: Float64 = CMTimeGetSeconds(avPlayer!.currentItem!.duration) - elapsedTime
        timeRemainingLabel.text = String(format: "%02d", (lround(timeRemaining) % 60))
    }
    
    private func observeTime(elapsedTime: CMTime) {
        let duration = CMTimeGetSeconds((avPlayer?.currentItem!.duration)!)
        if (!duration.isZero) {
            let elapsedTime = CMTimeGetSeconds(elapsedTime)
            updateTimeLabel(elapsedTime: elapsedTime, duration: duration)
        }
    }
    
    func setPlayerLayer(){
        self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
        
        let screenRect: CGRect = UIScreen.main.bounds
        
        self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: screenRect.size.width, height: screenRect.size.height)
    }

    
    func setExitButton(){
        self.exitButton.addTarget(self, action: #selector(exitButtonPressed(_:)), for: .touchUpInside)
        self.exitButton.frame = self.view.frame
    }
    
    
    func exitButtonPressed(_ button: UIButton) {
        
        self.dismiss(animated: true, completion: {
            self.avPlayer?.removeTimeObserver(self.timeObserver)
            self.avPlayer?.pause()
            self.avPlayer?.replaceCurrentItem(with: nil)

        })
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    //TODO: Make the view controller call deinit
    deinit {
        avPlayer?.removeTimeObserver(timeObserver)
        avPlayer?.pause()
        avPlayer?.replaceCurrentItem(with: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
