//
//  CameraViewController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 9/4/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import LLSimpleCamera


//TODO: Add a camera timer
//TODO: Submit video to video player after done recording

class CameraViewController: UIViewController {

    var errorLabel = UILabel()
    var snapButton = UIButton()
    var backButton = UIButton()
    var switchButton = UIButton()
    var flashButton = UIButton()
    var settingsButton = UIButton()
    var cameraTimer: Timer = Timer()
    var timerLabel: TimerLabel = TimerLabel()
    var camera = LLSimpleCamera(videoEnabled: true)
    var screenRect: CGRect?
    var shapeLayer = CAShapeLayer()
    var timerValue = 10

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !UIDevice.isSimulator(){
            self.camera?.start()    
        }
        


    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.backgroundColor = UIColor.black
        self.screenRect = UIScreen.main.bounds

        self.setupCamera()
//        self.setupCameraGUI()

     }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.camera?.stop()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let screenWidth = self.view.bounds.width
        let screenHeight = self.view.bounds.height


        self.camera?.view.frame = self.view.bounds

        self.snapButton.center = self.view.center
        self.snapButton.frame.origin.y = self.view.bounds.height - 90

        self.backButton.frame.origin.x = screenWidth - (0.95 * screenWidth)
        self.backButton.frame.origin.y = 5.0

        self.switchButton.frame.origin.x = screenWidth - (0.15 * screenWidth)
        self.switchButton.frame.origin.y = 5.0

        self.flashButton.frame.origin.x = screenWidth - (0.3 * screenWidth)
        self.flashButton.frame.origin.y = 5.0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return .portrait
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }



}

extension CameraViewController{


    func cancelButtonPressed(_ button: UIButton) {
        //self.navigationController!.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

    func jumpSettingsButtonPressed(_ button: UIButton) {
        UIApplication.shared.openURL(URL(string:UIApplicationOpenSettingsURLString)!)
    }



    func flashButtonPressed(_ button: UIButton) {
        if self.camera?.flash == LLCameraFlashOff {
            let done: Bool = self.camera!.updateFlashMode(LLCameraFlashOn)
            if done {
                self.flashButton.isSelected = true
            }
        } else {
            let done: Bool = self.camera!.updateFlashMode(LLCameraFlashOff)
            if done {
                self.flashButton.isSelected = false
            }
        }
    }

    func setupCamera() {
        if !UIDevice.isSimulator(){
            self.camera = LLSimpleCamera(quality: AVCaptureSessionPresetHigh, position: LLCameraPositionFront, videoEnabled: true)
            self.camera?.attach(to: self, withFrame: CGRect(x: 0, y: 0, width: screenRect!.size.width, height: screenRect!.size.height))
            self.camera?.fixOrientationAfterCapture = true
            
            
            self.camera?.onDeviceChange = {(camera, device) -> Void in
                if (camera?.isFlashAvailable())! {
                    self.flashButton.isHidden = false
                    if camera?.flash == LLCameraFlashOff {
                        self.flashButton.isSelected = false
                    } else {
                        self.flashButton.isSelected = true
                    }
                } else {
                    self.flashButton.isHidden = true
                }
            }
            
            self.handleCameraError()
        }
        self.setupCameraGUI()
    }

    func applicationDocumentsDirectory() -> URL {
        return FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).last!
    }

    func setSnapButtonAppearance(){
        self.snapButton = UIButton(type: .custom)
        self.snapButton.frame = CGRect(x: 0, y: 0, width: 70.0, height: 70.0)
        self.snapButton.clipsToBounds = true
        self.snapButton.layer.cornerRadius = self.snapButton.frame.width / 2.0
        self.snapButton.setBackgroundColor(color: kThemeMainColor.withAlphaComponent(0.6), forState: .normal)
        self.snapButton.setBackgroundColor(color: UIColor.flatRed().withAlphaComponent(0.6), forState: .selected)

        self.snapButton.layer.rasterizationScale = UIScreen.main.scale
        self.snapButton.layer.shouldRasterize = true
        self.snapButton.addTarget(self, action: #selector(snapButtonHold(_:)), for: .touchDown)
        self.snapButton.addTarget(self, action: #selector(snapButtonRelease(_:)), for: .touchUpInside)
        self.view!.addSubview(self.snapButton)
    }
    
    func setBackButton(){
        self.backButton = UIButton(type: .system)
        self.backButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        self.backButton.tintColor = UIColor.white
        self.backButton.setImage(#imageLiteral(resourceName: "cross-cancel-icon"), for: UIControlState())
        self.backButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.backButton.addTarget(self, action: #selector(CameraViewController.cancelButtonPressed(_:)), for: .touchUpInside)
        self.view!.addSubview(self.backButton)
    }
    func setFlashButton(){
        self.flashButton = UIButton(type: .system)
        self.flashButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        self.flashButton.setImage(#imageLiteral(resourceName: "Flash Off").withRenderingMode(.alwaysTemplate), for: .normal)
        self.flashButton.setImage(#imageLiteral(resourceName: "Flash On").withRenderingMode(.alwaysTemplate), for: .selected)
        self.flashButton.tintColor = kThemeMainColor
        self.flashButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.flashButton.addTarget(self, action: #selector(CameraViewController.flashButtonPressed(_:)), for: .touchUpInside)
        self.view!.addSubview(self.flashButton)
    }
    func setSwitchButton(){
        self.switchButton = UIButton(type: .system)
        self.switchButton.frame = CGRect(x: 0, y: 0, width: 44.0, height: 44.0)
        self.switchButton.setImage(#imageLiteral(resourceName: "Switch Camera").withRenderingMode(.alwaysTemplate), for: UIControlState())
        self.switchButton.tintColor = kThemeMainColor
        self.switchButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.switchButton.addTarget(self, action: #selector(CameraViewController.switchButtonPressed(_:)), for: .touchUpInside)
        self.view!.addSubview(self.switchButton)
    }
    
    func setTimerLabel(){
        self.timerLabel = TimerLabel(color: kThemeBrighterColor)
        self.timerLabel.isHidden = true
        self.view.addSubview(timerLabel)
        self.timerLabel.snp.makeConstraints{(make) -> Void in
            make.bottom.equalTo(self.snapButton.snp.top).offset(-10)
            make.centerX.equalTo(self.view.snp.centerX)
            make.width.equalTo(self.snapButton.snp.width)
            make.height.equalTo(self.snapButton.snp.height).dividedBy(4)
        }
    }
    
    func switchButtonPressed(_ button: UIButton) {
        if(camera?.position == LLCameraPositionRear) {
            self.flashButton.isHidden = false
        } else {
            self.flashButton.isHidden = true
        }
        
        self.camera?.togglePosition()
    }
    
    
    func snapButtonHold(_ button: UIButton) {
        
        startAnimation()
        self.timerLabel.setTimer(value: timerValue)
        self.switchButton.isHidden = true
        self.timerLabel.isHidden = !self.timerLabel.isHidden
        //        self.snapButton.layer.borderColor = UIColor.red.cgColor
        if !UIDevice.isSimulator(){
            if(!(camera?.isRecording)!) {
                if(self.camera?.position == LLCameraPositionRear && !self.flashButton.isHidden) {
                    self.flashButton.isHidden = true
                }
                // start recording
                let outputURL: URL = self.applicationDocumentsDirectory().appendingPathComponent("video").appendingPathExtension("mov")
//                let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//                let filePath = documentsURL.appendingPathComponent("temp")
                self.camera?.startRecording(withOutputUrl:outputURL) {(camera, outputFileUrl, error) -> Void in
                    let vc: PreviewVideoViewController = PreviewVideoViewController(videoUrl: outputFileUrl!)
                    self.navigationController!.pushViewController(vc, animated: true)
                    //self.setSnapButtonAppearance()
                }
            }
        }
        
    }
    func snapButtonRelease(_ button: UIButton){
        log.info("Released")
        cameraTimer.invalidate()
        stopCircle()
        self.timerLabel.isHidden = !self.timerLabel.isHidden
        self.switchButton.isHidden = false
        if !UIDevice.isSimulator(){
            if(self.camera?.position == LLCameraPositionRear && self.flashButton.isHidden) {
                self.flashButton.isHidden = false
            }
            self.camera?.stopRecording()
        }
    }
    
    func drawCircle(color: UIColor, width: CGFloat, view: UIView) {
        self.shapeLayer.isHidden = true
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: view.center.x  ,y: view.center.y), radius: view.frame.width / 2, startAngle: CGFloat(-M_PI_2), endAngle:CGFloat(2*M_PI-M_PI_2), clockwise: true)
        self.shapeLayer.path = circlePath.cgPath
        self.shapeLayer.fillColor = UIColor.clear.cgColor
        self.shapeLayer.strokeColor = color.cgColor
        self.shapeLayer.lineWidth = width
        self.shapeLayer.rasterizationScale = UIScreen.main.scale
        self.shapeLayer.shouldRasterize = true
        self.shapeLayer.name = "circleLine"
        view.layer.addSublayer(self.shapeLayer)
    }
    
    func updateCounterLabel(){
        log.info("Holding")
    }
    
    
    func stopCircle(){
        self.shapeLayer.isHidden = !self.shapeLayer.isHidden
        self.shapeLayer.removeAnimation(forKey: "ani")
        
    }
    
    func startAnimation() {
        self.shapeLayer.isHidden = !self.shapeLayer.isHidden
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = Double(self.timerValue)
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        self.shapeLayer.add(animation, forKey: "ani")
    }


    func setupCameraGUI() {
        if(LLSimpleCamera.isFrontCameraAvailable() && LLSimpleCamera.isRearCameraAvailable()) {
            setSnapButtonAppearance()
            drawCircle(color: kThemeBrighterColor, width: 10, view: self.snapButton)
            setTimerLabel()
            setSwitchButton()
            setBackButton()
            setFlashButton()
        }
        if(UIDevice.isSimulator()){
            let label: UILabel = UILabel(frame: CGRect.zero)
            label.text = "You must have a camera to take video."
            label.numberOfLines = 2
            label.lineBreakMode = .byWordWrapping
            label.backgroundColor = UIColor.clear
            label.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
            label.textColor = UIColor.white
            label.textAlignment = .center
            label.sizeToFit()
            label.center = CGPoint(x: screenRect!.size.width / 2.0, y: screenRect!.size.height / 2.0)
            self.errorLabel = label
            self.view!.addSubview(self.errorLabel)
        }
    }

    func handleCameraError() {
        self.camera?.onError = {(camera, error) -> Void in
            if ((error) != nil) {
                //                if error.code == 10 || error.code == 11 {
                if(self.view.subviews.contains(self.errorLabel)) {
                    self.errorLabel.removeFromSuperview()
                }

                let label: UILabel = UILabel(frame: CGRect.zero)
                label.text = "We need permission for the camera and microphone."
                label.numberOfLines = 2
                label.lineBreakMode = .byWordWrapping
                label.backgroundColor = UIColor.clear
                label.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
                label.textColor = UIColor.white
                label.textAlignment = .center
                label.sizeToFit()
                label.center = CGPoint(x: self.screenRect!.size.width / 2.0, y: self.screenRect!.size.height / 2.0)
                self.errorLabel = label
                self.view!.addSubview(self.errorLabel)

                let jumpSettingsBtn: UIButton = UIButton(frame: CGRect(x:50, y:label.frame.origin.y + 50, width: self.screenRect!.size.width - 100, height: 50))
                jumpSettingsBtn.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 24.0)
                jumpSettingsBtn.setTitle("Go Settings", for: .normal)
                jumpSettingsBtn.setTitleColor(UIColor.white, for: .normal)
                jumpSettingsBtn.layer.borderColor = UIColor.white.cgColor
                jumpSettingsBtn.layer.cornerRadius = 5
                jumpSettingsBtn.layer.borderWidth = 2
                jumpSettingsBtn.clipsToBounds = true
                jumpSettingsBtn.addTarget(self, action: #selector(CameraViewController.jumpSettingsButtonPressed(_:)), for: .touchUpInside)
                jumpSettingsBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center

                self.settingsButton = jumpSettingsBtn

                self.view!.addSubview(self.settingsButton)

                self.switchButton.isEnabled = false
                self.flashButton.isEnabled = false
                self.snapButton.isEnabled = false
            }
        }
    }

}
