//
//  UITableViewCell+Mouve.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/19/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell{
    static func arrowedCell(title: String, mainColor: UIColor) -> UITableViewCell{
        let cell = UITableViewCell()
        cell.arrowedCell(title: title, mainColor: mainColor)
        return cell
    }
    
    func arrowedCell(title: String, mainColor: UIColor){
        let arrow = UIImageView(image: #imageLiteral(resourceName: "Arrow").withRenderingMode(.alwaysTemplate))
        arrow.frame = CGRect(x: 0.0, y: 0.0, width: arrow.frame.width * 0.2, height: arrow.frame.height * 0.2)
        self.textLabel?.text = title
        self.textLabel?.textColor = mainColor
        self.accessoryType = .disclosureIndicator
        self.accessoryView = arrow
        self.accessoryView?.tintColor = mainColor
    }
}
