//
//  DGTAppearance+Mouve.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 10/19/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import DigitsKit

extension DGTAppearance{
    class func setDigitsAppearance(mainColor: UIColor) -> DGTAppearance{
        let digitsAppearance = DGTAppearance()
        digitsAppearance.accentColor = mainColor
        digitsAppearance.logoImage = #imageLiteral(resourceName: "mouve-icon")
        digitsAppearance.backgroundColor = UIColor.white
        return digitsAppearance
    }

}
