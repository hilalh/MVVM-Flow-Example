//
//  PingPopupView.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/7/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Nuke
import XLActionController

class PingPopupView: UIViewController {
    var viewModel: PingViewModel?
    var profileImageView = RoundableUIImageView()
    var nameLabel = UILabel()
    var usernameLabel = UILabel()
    var inviteButton = UIButton()
    var createButton = UIButton()
    var ignoreButton = UIButton()
    var container = UIView()
    
    convenience init(_viewModel: PingViewModel) {
        self.init()
        viewModel = _viewModel
        try! setMainView(picURL: (viewModel?.picURL.value())!, name: (viewModel?.fullName.value())!, username: (viewModel?.fullName.value())!)
    }
    
    func setMainView(picURL: URL, name: String, username: String){
        self.view.addSubview(usernameLabel)
        usernameLabel.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(self.view.snp.width)
            make.height.equalTo(self.view.snp.height).multipliedBy(0.20)
            make.top.equalTo(self.view.snp.top).offset(5)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        usernameLabel.text = "PINGED BY: @\(username)"
        usernameLabel.textColor = UIColor.flatRed()
        usernameLabel.textAlignment = .center
        usernameLabel.adjustsFontSizeToFitWidth = true
        usernameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        
        
        self.view.addSubview(container)
        container.snp.makeConstraints{ (make) -> Void in
            make.height.equalTo(self.view.snp.height).multipliedBy(0.35)
            make.width.equalTo(self.view.snp.width).multipliedBy(0.792)
            make.center.equalTo(self.view.snp.center)
        }
        container.backgroundColor = UIColor.white
        container.layer.cornerRadius = 4
        Nuke.loadImage(with: picURL, into: profileImageView)
        view.addSubview(profileImageView)
        profileImageView.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width).multipliedBy(0.4)
            make.height.equalTo(container.snp.width).multipliedBy(0.4)
            make.centerX.equalTo(container.snp.centerX)
            make.centerY.equalTo(container.snp.top)
        }
        profileImageView.borderWidth = 4
        profileImageView.borderColor = UIColor.white
        
        let moreBtnView = UIButton()
        container.addSubview(moreBtnView)
        moreBtnView.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(20)
            make.height.equalTo(4)
            make.top.equalTo(container.snp.top).offset(5)
            make.right.equalTo(container.snp.right).inset(5)
        }
        moreBtnView.setImage(#imageLiteral(resourceName: "xMore"), for: .normal)
        moreBtnView.tintColor = UIColor.darkGray
        moreBtnView.addTarget(self, action: #selector(openActionSheet), for: .touchUpInside)

        container.addSubview(nameLabel)
        nameLabel.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width)
            make.height.equalTo(container.snp.height).multipliedBy(0.15)
            make.top.equalTo(profileImageView.snp.bottom).offset(3)
            make.centerX.equalTo(container.snp.centerX)
        }
        nameLabel.text = name
        nameLabel.textAlignment = .center
        nameLabel.font = UIFont.preferredFont(forTextStyle: .subheadline)
        container.addSubview(inviteButton)
        inviteButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width).multipliedBy(0.5)
            make.height.equalTo(container.snp.height).multipliedBy(0.15)
            make.top.equalTo(nameLabel.snp.bottom).offset(3)
            make.centerX.equalTo(container.snp.centerX)
        }
        inviteButton.setTitle("Invite them", for: .normal)
        inviteButton.backgroundColor = UIColor.flatBlue()
        inviteButton.layer.cornerRadius = 4
        inviteButton.titleLabel?.textAlignment = .center
        inviteButton.titleLabel?.textColor = UIColor.white
        inviteButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        inviteButton.addTarget(self, action: #selector(inviteTapped), for: .touchUpInside)
        container.addSubview(createButton)
        createButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width).multipliedBy(0.5)
            make.height.equalTo(container.snp.height).multipliedBy(0.15)
            make.top.equalTo(inviteButton.snp.bottom).offset(3)
            make.centerX.equalTo(container.snp.centerX)
        }
        createButton.setTitle("Create a Mouve", for: .normal)
        createButton.backgroundColor = kThemeBrighterColor
        createButton.layer.cornerRadius = 4
        createButton.titleLabel?.textAlignment = .center
        createButton.titleLabel?.textColor = UIColor.white
        createButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        createButton.addTarget(self, action: #selector(createMouveTapped), for: .touchUpInside)
        container.addSubview(ignoreButton)
        ignoreButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(container.snp.width).multipliedBy(0.5)
            make.height.equalTo(container.snp.height).multipliedBy(0.15)
            make.top.equalTo(createButton.snp.bottom).offset(3)
            make.centerX.equalTo(container.snp.centerX)
        }
        ignoreButton.setTitle("Ignore", for: .normal)
        ignoreButton.backgroundColor = UIColor.lightGray
        ignoreButton.layer.cornerRadius = 4
        ignoreButton.titleLabel?.textColor = UIColor.white
        ignoreButton.titleLabel?.textAlignment = .center
        ignoreButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        ignoreButton.addTarget(self, action: #selector(ignoreTapped), for: .touchUpInside)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let exitButton = UIButton()
        self.view.addSubview(exitButton)
        exitButton.snp.makeConstraints{ (make) -> Void in
            make.width.equalTo(self.view.snp.width).multipliedBy(0.1)
            make.height.equalTo(self.view.snp.width).multipliedBy(0.1)
            make.bottom.equalTo(self.view.snp.bottom).inset(10)
            make.centerX.equalTo(self.view.snp.centerX)
        }
        exitButton.setImage(#imageLiteral(resourceName: "slickX").withRenderingMode(.alwaysTemplate), for: .normal)
        exitButton.tintColor = kThemeBrighterColor
        exitButton.addTarget(self, action: #selector(dismissVC), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        profileImageView.round = true
    }
    
    func dismissVC(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func ignoreTapped(){
        dismissVC()
    }
    
    func createMouveTapped(){
        log.info("Create a Mouve tapped")
    }
    
    func inviteTapped(){
        log.info("Invite Tapped")
    }
    
    func openActionSheet(){
        let actionController = MouveActionController()
        actionController.headerData = "What would you like to do?"
        /*
        actionController.addAction(Action("Invite more people", style: .default, handler: { action in
            log.info("Invite to Mouve")
            self.show(UINavigationController(rootViewController: InviteListViewController()), sender: self)
        }))
         */
        actionController.addAction(Action("Report this user", style: .destructive, handler: { action in
            //self.mouveViewModel?.reportMouve()
            log.info("Report: \(self.viewModel)")
        }))
        actionController.addAction(Action("Cancel", style: .cancel, handler:nil))
        
        self.present(actionController, animated: true, completion: nil)
        
    }

}
