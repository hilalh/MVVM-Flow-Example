//
//  ErrorHandler.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import SwiftMessages

public enum MessageType{
    case Success
    case Error
    case Warning
    case Info
}

class ErrorHandler {
    class func showMessage(text: String, type: MessageType = .Error){
        switch type{
        case .Success:
            self.showSuccessMessage(text: text)
        case .Warning:
            self.showWarningMessage(text: text)
        case .Info:
            self.showInfoMessage(text: text)
        case .Error:
            self.showErrorMessage(text: text)
        }
        self.logOnly(text: text, type: type)
    }
    class func logOnly(text: String, type: MessageType = .Error){
        switch type{
        case .Success:
            log.info("Success: \(text)")
        case .Warning:
            log.warning(text)
        case .Info:
            log.info(text)
        case .Error:
            log.error(text)
        }
    }
    
    private class func showErrorMessage(text: String){
        SwiftMessages.show {
            let mssg = MessageView.viewFromNib(layout: .StatusLine)
            mssg.configureTheme(.error)
            mssg.configureContent(title: "Oops.", body: text, iconText: "😡")
            return mssg
        }
    }
    
    private class func showWarningMessage(text: String){
        SwiftMessages.show {
            let mssg = MessageView.viewFromNib(layout: .StatusLine)
            mssg.configureTheme(.warning)
            mssg.configureContent(title: "Hmm...", body: text, iconText: "😒")
            return mssg
        }
    }
    
    private class func showSuccessMessage(text: String){
        SwiftMessages.show {
            let mssg = MessageView.viewFromNib(layout: .StatusLine)
            mssg.configureTheme(.success)
            mssg.configureContent(title: "Yay!", body: text, iconText: "😇")
            return mssg
        }
    }
    
    private class func showInfoMessage(text: String){
        SwiftMessages.show {
            let mssg = MessageView.viewFromNib(layout: .StatusLine)
            mssg.configureTheme(.info)
            mssg.configureContent(title: "Attention!", body: text, iconText: "🤔")
            return mssg
        }
    }
}
