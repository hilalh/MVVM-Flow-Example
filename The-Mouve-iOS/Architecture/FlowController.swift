//
//  FlowController.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/23/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
//TODO: Add Flow Controller for Mouve Cards
//TODO: Add Flow Controller for Sign Up / Details screen
//TODO: Add Flow Controller for Profile Screen Table
//TODO: Add Flow Controller for Login
enum FlowType {
    case Main
    case Popup
    case Navigation
}

struct FlowConfigure {
    let window : UIWindow?
    let navigationController : UINavigationController?
    let parent : FlowController?
    
    func whichFlowIam() -> FlowType? {
        if window != nil { return .Main }
        if navigationController != nil { return .Navigation }
        return nil
    }
}

protocol FlowController {
    init(configure : FlowConfigure)
    func start()
}
