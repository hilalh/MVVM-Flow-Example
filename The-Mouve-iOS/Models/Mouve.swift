//
//  Mouve.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/25/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation

protocol Mouve {
    var key: String { get }
    var creator: String { get }
    var name: String { get }
    var address: String { get }
    var startTime: String { get }
    var endTime: String { get }
    var privacy: String { get }
    var disabled: String { get }
    var locationLat: String { get }
    var locationLong: String { get }
    var videoURL: String? { get }
    var thumbnailImgURL: String? { get }
}
