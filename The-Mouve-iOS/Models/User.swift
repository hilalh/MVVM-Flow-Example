//
//  User.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/25/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation

protocol User {
    var uid: String { get }
    var email: String { get }
    var fullName: String { get }
    var username: String { get }
    var gender: String { get }
    var DOB: Date { get }
    var profileImageURL: String? { get }
    var phoneNumberNational: String { get }
    var phoneNumberInternational: String { get }
    var verificationCode: String? { get }
}
