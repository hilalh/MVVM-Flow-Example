//
//  Ping.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/8/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation

protocol Ping {
    associatedtype abstractUser: User
    var key: String { get }
    var creator: abstractUser { get }
    var targetUser: abstractUser { get }
    var timeStamp: String { get }
    var sendingLocationLat: String { get }
    var sendingLocationLong: String { get }
}
