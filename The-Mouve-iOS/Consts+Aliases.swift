//
//  Consts+Aliases.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 8/25/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import UIKit

//typealias JSON = [String: Any]

typealias dataManager = FirebaseUtility
typealias abstractMouve = FMouve
typealias abstractUser = FUser
typealias abstractPing = FPing

public enum ScenePageModes{
    case allMouves
    case checkedInMouves
}
//Firebase stuff
let kFirebasePhone = "phoneNumber"
let kFirebaseUsername = "username"
let kFirebaseMouveStorageURL = "gs://the-mouve-2eb25.appspot.com"


//Colors
let kThemeBrighterColor: UIColor = UIColor.flatMint().lighten(byPercentage: 0.3)
let kThemeMainColor: UIColor = UIColor.flatMint().lighten(byPercentage: 0.1)




extension UIApplication{
    func loginVC() -> UIViewController{
        return LoginViewController()
    }
    func MainVC() -> UIViewController
    {
        //        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        //        var vc = storyboard.instantiateInitialViewController() as! CenterButtonTabBarViewController
        //        return vc
        
        let navscene = ScenePageViewController(viewMode: .allMouves)
        //HomeViewController()
        //let scene = ScenePageViewController(viewMode: .allMouves)
//        let navscene = UINavigationController(rootViewController: scene)
        //        let navscene = scene
        //navscene.tabBarItem.title = "Scene"
        navscene.tabBarItem.image = #imageLiteral(resourceName: "thescene-icon")
        
        
        
        let contacts = ContactListViewController()
        let navcontacts = contacts
        //navcontacts.tabBarItem.title = "Contact List"
        navcontacts.tabBarItem.image = #imageLiteral(resourceName: "profile-icon")
        
        
        
        let tabs = CenterButtonTabBarViewController()
        tabs.tabBar.barTintColor = UIColor.white
        tabs.viewControllers = [navscene, navcontacts]
        //        let nav = UINavigationController(rootViewController: tabs)
        //        tabs.viewControllers = [nav1, center, nav2]
        return tabs
    }

}
