Git Guidelines:
1. Fixing feature:
- When fixing a feature, make a new branch called "fix.{the-feature-being-fixed}"
2. Adding feature:
- When adding a feature, make a new branch called "add.{the-feature-being-added}"
* When done please make a merge request to "ui", "backend", or, "misc"

Code Guidelines:
1. Styling of basic code concepts
- https://github.com/raywenderlich/swift-style-guide
2. Github styling of more advanced concepts
- https://github.com/github/swift-style-guide

Error Handling 101:
1. Error Handling per RayWenderlich
- https://www.raywenderlich.com/130197/magical-error-handling-swift

General Guidelines:
1. Models
  - Contain and structure data
  - Each model has a specific version and an abstract protocol
2. View Models
  - Link between Models and View Controllers
  - Help structure data and format data for the view Controllers
  - Contain business logic that allows us to manipulate the data
  - Being called by View Controllers, but do not call view Controllers
  - Make calls to network data managers
  - Usually linked to small - medium size UI components
3. Data managers
  - Contain and make calls to many view models
  - Usually used in large scenes (ex. contact list page, scene page)
4. Components:
  - Should be passed a view model instead of data unless there are too many use cases for the component (5+)
5. Fastlane and Automatic deployment
  - "fastlane local" will deploy the build on your phone
  - "fastlane beta" will deploy the build on Crashlytics beta
  - Guide: https://hacking-ios.cocoagarage.com/fastlane-crashlytics-beta-slack-awesome-364f6453f8ab#.qwt5scu78

Tips & Tricks:
1. If keyboard doesn't show, make sure you press Cmd + K
