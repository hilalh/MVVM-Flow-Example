
//  ParseUtility.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 8/22/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import FacebookCore
import FacebookLogin
//import KeychainAccess
import UIKit
import Firebase
import FirebaseStorage



class FirebaseUtility {
    static let shared : FirebaseUtility = {
        let instance = FirebaseUtility()
        instance.configureDatabase()
        instance.configureStorage()
        return instance
    }()
    
    static let userOps : FirebaseUserUtility = {
        let instance = FirebaseUserUtility()
        return instance
    }()
    
    static let facebook : FacebookManager = {
        let instance = FacebookManager()
        return instance
    }()
    
    static let fakeData : FakeData = {
        let instance = FakeData()
        return instance
    }()
    
    var storageRef: FIRStorageReference?
    var dbRef: FIRDatabaseReference?
    func configureStorage() {
        storageRef = FIRStorage.storage().reference()
    }
    func configureDatabase() {
        dbRef = FIRDatabase.database().reference()
    }
}

extension FirebaseUtility: MouveNetworkActions {
    func retrieveMouveInvites(mouves: [FMouve], onCompletion: ()-> ()) {
        onCompletion()
//        dataManager.userOps
    }

    func createMouveInBackground(_ creator: String, name: String, address: String, startTime: String, endTime: String, privacy: String, locationLat: String, locationLong: String, videoURL: String, thumbnailImgURL: String, onCompletion: ((_ error: NSError?, _ ref: FIRDatabaseReference) -> ())?) {

        var dbRef: FIRDatabaseReference!
        dbRef = FIRDatabase.database().reference().child("Mouve")
        let objectRef = dbRef.child(name)

        let fpEvent = FMouve.init(creator: creator, name: name, address: address, startTime: startTime, endTime: endTime, privacy: privacy, locationLat: locationLat, locationLong: locationLong, videoURL: videoURL, thumbnailImgURL: thumbnailImgURL)


        objectRef.setValue(fpEvent.toAnyObject(), withCompletionBlock: onCompletion! as! (Error?, FIRDatabaseReference) -> Void)

    }

    
    func uploadMouveFile(name: String, format: String, contentType: String, key: String, fileURL: URL, onCompletion: @escaping (FIRStorageMetadata?, Error?) -> ()) -> FIRStorageUploadTask{
        let fileRef = FIRStorage.storage().reference(withPath: "files/mouve_\(key)/\(name).\(format)")
        let metaData = FIRStorageMetadata()
        metaData.contentType = "\(contentType)/\(format)"
        return fileRef.putFile(fileURL, metadata: metaData){ (metadata, error) in
            onCompletion(metadata, error)
        }
    //        TODO: Figure out a way to report multiple progresses
    //        TODO: Finish the Firebase file upload function

    }
    
    
    func invitePeopleToMouve(_ mouve: FMouve, people: [FUser]) {
     //        TODO: Finish the Firebase invite function
    }

    func deleteMouveInBackground(_ mouve: FMouve) {
     //        TODO: Finish the Firebase delete mouve function
    }
//Toggles attending a Mouve
    func toggleAttendMouve(_ mouve: FMouve) {
     //        TODO: Finish the Firebase attend mouve function based on the code below
    }



}
