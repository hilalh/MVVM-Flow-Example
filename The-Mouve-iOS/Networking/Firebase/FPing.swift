
//
//  FPing.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/8/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import Firebase
import CoreLocation
import SwiftDate


protocol PingNetworkActions {
    func pingUser(_ targetUser: User)
}

struct FPing: Ping {
    
    
    var key: String
    var creator: abstractUser
    var targetUser: abstractUser
    var timeStamp: String
    var sendingLocationLat: String
    var sendingLocationLong: String
    var itemRef: FIRDatabaseReference?
    
    /*
    static func demo(sourceUser: User) -> FUser {
        var ping = self.init()
        /*
        
        user.fullName = fullName
        user.username = username
        user.profileImageURL = picURL
        
         */
        return ping
    }
     */
    //Basic example init method creates a ping with now-time and location of 3rd St Promenande SaMo
    init(creator: abstractUser, targetUser: abstractUser, timeStamp: Date = Date(), fromLocation: CLLocation = CLLocation(latitude: 34.0160313, longitude: -118.4981864)) {
        self.key = ""
        self.creator = creator
        self.targetUser = targetUser
        self.timeStamp = DateInRegion(absoluteDate: timeStamp).iso8601()
        try! self.sendingLocationLat = fromLocation.coordinate.latitude.string()
        try! self.sendingLocationLong = fromLocation.coordinate.longitude.string()
        self.itemRef = FIRDatabaseReference()
    }
    //TODO: Figure how to get the sender user and all its details
    /*
    init(key: String = "", creator: String = "", targetUser: String = "", timeStamp: String = "", sendingLocationLat: String = "", sendingLocationLong: String = "") {
        
        self.key = key
        self.creator = creator
        self.name = name
        self.address = address
        self.startTime = startTime
        self.endTime = endTime
        self.privacy = privacy
        self.locationLat = locationLat
        self.locationLong = locationLong
        self.backgroundImageURL = backgroundImageURL
        self.thumbnailImgURL = thumbnailImgURL
        self.itemRef = nil
        self.disabled = ""
 
        
    }
     */
}
