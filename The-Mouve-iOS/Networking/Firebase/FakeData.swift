//
//  TestData.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/22/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit

class FakeData {
    var users: [FUser] {
        var users = [FUser]()
        let marco = FUser(username: "mf1529", fullName: "Marco Fabrega", picURL: "https://scontent-ort2-1.xx.fbcdn.net/t31.0-8/14500483_10207471476446210_6557242980578298192_o.jpg")
        /*
         let uduimoh = FUser(username: "finding_duimoh", fullName: "Uduimoh Umolu", picURL: "https://scontent-ort2-1.xx.fbcdn.net/t31.0-8/13029489_10209376317232849_2595849459925989072_o.jpg")
         */
        let themouve = FUser(username: "themouve", fullName: "The Mouve", picURL: "https://scontent-ort2-1.xx.fbcdn.net/v/t1.0-1/p320x320/12439059_1697883227162770_1432690694534869966_n.png?oh=abe134827b60dc9ee6ef8defa521039c&oe=588CD936")
        users.append(marco)
        users.append(themouve)
        return users
    }
    
    var pings: [FPing] {
        let marco = FPing(creator: self.users[0], targetUser: users[0])
        //let uduimoh = FPing(creator: self.users[1], targetUser: myUser)
        let themouve = FPing(creator: self.users[1], targetUser: users[0])
        return [marco, themouve]
    }
    
    var mouves: [FMouve] {
        let samoMouve: FMouve = FMouve(creator: "Hilal", name: "#WhatsTheMouve", address: "3rd St Promenande, Santa Monica, CA", startTime: "3:00AM", endTime: "6:00AM", privacy: "false", locationLat: "-3.00", locationLong: "33.00", videoURL: "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4", thumbnailImgURL: "https://scontent-ort2-1.xx.fbcdn.net/t31.0-8/14361357_10154565938829324_5156130571627955907_o.jpg"
        )
        
        return [samoMouve,samoMouve,samoMouve]
    }
    
    var attendents: [FUser] {
        let marco = FUser(username: "mf1529", fullName: "Marco Fabrega", picURL: "https://scontent-ort2-1.xx.fbcdn.net/t31.0-8/14500483_10207471476446210_6557242980578298192_o.jpg")
        
        let uduimoh = FUser(username: "finding_duimoh", fullName: "Uduimoh Umolu", picURL: "https://scontent-ort2-1.xx.fbcdn.net/t31.0-8/13029489_10209376317232849_2595849459925989072_o.jpg")
        
        return [marco, uduimoh]
    }

}
