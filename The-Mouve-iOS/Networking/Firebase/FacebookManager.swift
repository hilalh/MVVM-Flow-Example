//
//  FacebookManager.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/22/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import Unbox

class FacebookManager {
    var loginManager: LoginManager = LoginManager()
    let usernameRef = dataManager.shared.dbRef?.child("usernames")
    let userRef = dataManager.shared.dbRef?.child("users")
}

extension FacebookManager {
    
    func fetchDetails(onCompletion: @escaping ((FacebookProfile?, Error?) -> ())){
        let params = ["fields": "id, name, birthday, email, gender"]
        let graphRequest = GraphRequest(graphPath: "me", parameters: params)
        graphRequest.start{(urlResponse, requestResult) in
            switch requestResult {
            case .failed(let error):
                ErrorHandler.showMessage(text: kErrorGraphRequest)
                onCompletion(nil, error)
                break
            case .success(let response):
                guard let dict = response.dictionaryValue else{
                    ErrorHandler.logOnly(text: kErrorEmptyDict)
                    return
                }
                guard let prof: FacebookProfile = try? unbox(dictionary: dict) else{
                    ErrorHandler.logOnly(text: kErrorUnboxJSON)
                    return
                }
                onCompletion(prof, nil)
            }
        }
    }
    
    func loginGateway(onCompletion: @escaping (AccessToken?, Error?) -> ()){
        guard let accessToken = AccessToken.current else{
            loginManager.logIn([ .publicProfile, .email, .custom("user_birthday") ], viewController: UIApplication.shared.keyWindow?.rootViewController) { loginResult in
                switch loginResult {
                case .failed(let error):
                    log.error("Facebook login failed. Error \(error)")
                    onCompletion(nil, error)
                case .cancelled:
                    log.warning("Facebook login was cancelled.")
                    onCompletion(nil, nil)
                case .success( _, _, let accessToken):
                    log.info("Logged in!")
                    onCompletion(accessToken, nil)
                }
            }
            return
        }
        onCompletion(accessToken, nil)
    }

}
