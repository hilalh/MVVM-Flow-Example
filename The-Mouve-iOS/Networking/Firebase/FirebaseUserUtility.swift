//
//  FirebaseUserUtility.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/21/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import UIKit
import Firebase
import FacebookCore
import FacebookLogin

class FirebaseUserUtility {
    var loginManager: LoginManager = LoginManager()
    let usernameRef = dataManager.shared.dbRef?.child("usernames")
    let userRef = dataManager.shared.dbRef?.child("users")
}

extension FirebaseUserUtility: UserNetworkActions {
    
    //TODO: Finish function that allows us to check whether user is new
    func isUserFieldExisting(fieldName: String, onCompletion: @escaping (_ result: Bool) -> ()) {
        guard let userID = FIRAuth.auth()?.currentUser?.uid else { return }
        userRef?.child(userID).child(fieldName).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            guard snapshot.exists() else{
                onCompletion(false)
                return
            }
            onCompletion(true)
        })
    }
    
    func isUsernameAvailable(username: String,onCompletion: @escaping (_ result: Bool) -> ()) {
        usernameRef?.child(username).observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            guard snapshot.exists() else{
                onCompletion(true)
                return
            }
            onCompletion(false)
        })
    }
    func setUserProfile(onCompletion: @escaping (FIRUser?, Error?)->()) {
        //        TODO: Finish the function that sets the user details from fetchFacebookDetails
        
    }

    func setUsername(username: String, onCompletion: @escaping (Bool, Error?)->()){
        
        guard let uid = FIRAuth.auth()?.currentUser?.uid else { return }
        let dataRef = dataManager.shared.dbRef
        dataRef?.runTransactionBlock({ (currentData: FIRMutableData) -> FIRTransactionResult in
            
            let usernameData = currentData.childData(byAppendingPath: "usernames").childData(byAppendingPath: username)
            let userData = currentData.childData(byAppendingPath: "users").childData(byAppendingPath: uid).childData(byAppendingPath: "username")
            usernameData.value = uid
            userData.value = username
            return FIRTransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            guard let error = error else{
                onCompletion(committed, nil)
                return
            }
            log.error(error)
            onCompletion(committed, error)
        }
    }
    
    
    func logOut(){
        log.info("Logging out")
        try! FIRAuth.auth()?.signOut()
        loginManager.logOut()
        AppState.sharedInstance.myUser = nil
        AppState.sharedInstance.signedIn = false
        UIApplication.shared.keyWindow?.rootViewController = UIApplication.shared.loginVC()
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
    
    func deleteAccount(){
        guard let accessToken = AccessToken.current?.authenticationToken else{
            log.warning("Couldn't get any tokens")
            return
        }
        let auth = FIRFacebookAuthProvider.credential(withAccessToken: accessToken)
        FIRAuth.auth()?.currentUser?.reauthenticate(with: auth, completion: { (error) in
            guard let error = error else {
                FIRAuth.auth()?.currentUser?.delete(completion: { (error) in
                    guard let error = error else {
                        self.logOut()
                        return
                    }
                    log.error(error)
                })
                return
            }
            log.error(error)
        })
    }
    //TODO: Finish function that allows us to set details of user
    func setDetails(userField: String, fieldValue: Any, onCompletion: @escaping (Bool, Error?)->()) {
        guard let userID = FIRAuth.auth()?.currentUser?.uid else { return }
        let fieldRef = dataManager.shared.dbRef?.child("users").child(userID).child(userField)
        fieldRef?.setValue(fieldValue){(error, ref) -> Void in
            guard let error = error else{
                onCompletion(true, nil)
                return
            }
            onCompletion(false, error)
            log.error(error)
        }
    }
    
    func signedIn(_ user: FIRUser?, onCompletion: (() -> ()?)) {
        AppState.sharedInstance.myUser = abstractUser(userData: user)
        AppState.sharedInstance.signedIn = true
        AppState.sharedInstance.logCrashes()
        onCompletion()
    }
    
    
    func authenticateUser(onCompletion: @escaping (FIRUser?, Error?)->()) {
        dataManager.facebook.loginGateway{ (accessToken, error) -> () in
            guard let accessToken = accessToken else{
                onCompletion(nil, error)
                return
            }
            let credential = FIRFacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
            FIRAuth.auth()?.signIn(with: credential) { (user, error) in
                onCompletion(user, error)
            }
        }
    }
}
