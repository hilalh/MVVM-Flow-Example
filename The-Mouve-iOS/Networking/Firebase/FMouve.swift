//
//  Event.swift
//  The Mouve
//
//  Created by Samuel Ojogbo on 8/18/15.
//  Copyright (c) 2015 The Mouve. All rights reserved.
//

import Foundation
import Firebase

protocol MouveNetworkActions {
    func retrieveMouveInvites(mouves: [FMouve], onCompletion: ()-> ())
    func toggleAttendMouve(_ mouve: FMouve)
    func deleteMouveInBackground(_ mouve: FMouve)
    func invitePeopleToMouve(_ mouve: FMouve, people: [FUser])
    //func uploadMouveFiles(_ mouve: FMouve)
    func createMouveInBackground(_ creator: String, name: String, address: String, startTime: String, endTime: String, privacy: String, locationLat: String, locationLong: String, videoURL: String, thumbnailImgURL: String, onCompletion: ((_ error: NSError?, _ ref: FIRDatabaseReference) -> ())?)
}

struct FMouve: Mouve {
    
    var key: String
    var creator: String
    var name: String
    var address: String
    var startTime: String
    var endTime: String
    var privacy: String
    var disabled: String
    var locationLat: String
    var locationLong: String
    var videoURL: String?
    var thumbnailImgURL: String?
    var itemRef: FIRDatabaseReference?


    init(key: String = "", creator: String, name: String, address: String, startTime: String, endTime: String, privacy: String, locationLat: String, locationLong: String, videoURL: String, thumbnailImgURL: String) {

        self.key = key
        self.creator = creator
        self.name = name
        self.address = address
        self.startTime = startTime
        self.endTime = endTime
        self.privacy = privacy
        self.locationLat = locationLat
        self.locationLong = locationLong
        self.videoURL = videoURL
        self.thumbnailImgURL = thumbnailImgURL
        self.itemRef = nil
        self.disabled = ""

    }

    init (snapshot: FIRDataSnapshot) {
        key = snapshot.key
        itemRef = snapshot.ref

        if let eventCreator = (snapshot.value! as AnyObject).object(forKey: "creator") as? String {
            creator = eventCreator
        } else { creator = ""}

        if let eventName = (snapshot.value! as AnyObject).object(forKey: "name") as? String {
            name = eventName
        } else { name = ""}

        if let eventCreator = (snapshot.value! as AnyObject).object(forKey: "address") as? String {
            address = eventCreator
        } else { address = ""}

        if let eventST = (snapshot.value! as AnyObject).object(forKey: "startTime") as? String {
            startTime = eventST
        } else { startTime = ""}

        if let eventET = (snapshot.value! as AnyObject).object(forKey: "endTime") as? String {
            endTime = eventET
        } else { endTime = ""}

        if let eventprivacy = (snapshot.value! as AnyObject).object(forKey: "privacy") as? String {
            privacy = eventprivacy
        } else { privacy = ""}

        if let eventlat = (snapshot.value! as AnyObject).object(forKey: "locationLat") as? String {
            locationLat = eventlat
        } else { locationLat = ""}

        if let eventLong = (snapshot.value! as AnyObject).object(forKey: "locationLong") as? String {
            locationLong = eventLong
        } else { locationLong = ""}

        if let eventVideo = (snapshot.value! as AnyObject).object(forKey: "videoURL") as? String {
            videoURL = eventVideo
        } else { videoURL = ""}

        if let eventBgThumb = (snapshot.value! as AnyObject).object(forKey: "thumbnailImgURL") as? String {
            thumbnailImgURL = eventBgThumb
        } else { thumbnailImgURL = ""}

        if let eventdisabled = (snapshot.value! as AnyObject).object(forKey: "disabled") as? String {
            disabled = eventdisabled
        } else { disabled = ""}

    }


    func toAnyObject() -> Any? {

        return ["creator": creator, "name": name, "address": address, "startTime": startTime, "endTime": endTime, "privacy": privacy, "locationLat": locationLat, "locationLong": locationLong, "videoURL": videoURL!, "thumbnailImgURL": thumbnailImgURL!, "disabled": disabled ]
    }
}
