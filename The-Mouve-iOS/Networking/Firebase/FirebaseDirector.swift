////
////  FirebaseDirector.swift
////  The-Mouve-iOS
////
////  Created by Hilal Habashi on 9/9/16.
////  Copyright © 2016 Hilal Habashi. All rights reserved.
////
//
//import Foundation
//
//final class FirebaseDirector: FirebaseDirectorProtocols {
//
//
//
//    // MARK: Properties
//
//    fileprivate let builder: FirebaseBuilder
//    fileprivate let manager: FirebaseManager
//
//    // MARK: Init
//
//    init(builder: FirebaseBuilder, databaseManager: FirebaseManager) {
//        self.builder = builder
//        self.manager = databaseManager
//    }
//
//    // MARK: FMouveDirectorProtocols
//
//    func allMouves(_ completion: @escaping ([FMouve]) -> ()) {
//        manager.allMouves { [unowned self] (snapshot) in
//            let mouves = self.builder.mouvesFromResponse(snapshot)
//            completion(mouves)
//        }
//    }
//
//    func checkedInMouves(_ completion: ([FMouve]) -> ()) {
//
//    }
//
//
//}
