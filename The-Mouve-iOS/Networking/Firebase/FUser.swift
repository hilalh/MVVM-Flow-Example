//
//  PFEUser.swift
//  The Mouve
//
//  Created by Hilal Habashi on 9/27/15.
//  Copyright © 2015 The Mouve. All rights reserved.
//

import Foundation
import FacebookCore
import Firebase
import SwiftDate
import Unbox

protocol UserNetworkActions {
    func isUserFieldExisting(fieldName: String, onCompletion: @escaping (_ result: Bool) -> ())
    func setUserProfile(onCompletion: @escaping (FIRUser?, Error?)->())
    func setDetails(userField: String, fieldValue: Any, onCompletion: @escaping (Bool, Error?)->())
    func authenticateUser(onCompletion: @escaping (FIRUser?, Error?)->())
}


struct FacebookProfile{
    let birthday: Date
    
    let email: String
    
    let gender: String
    
    let fbId: Int
    
    let name: String

}

extension FacebookProfile: Unboxable {
    init(unboxer: Unboxer) throws {
        self.birthday = try DateInRegion(string: unboxer.unbox(key: "birthday"), format: .custom("MM/dd/yyyy")).absoluteDate
        self.email = try unboxer.unbox(key: "email")
        self.gender = try String(string: unboxer.unbox(key: "gender"))!.capitalized
        self.fbId = try Int(string: unboxer.unbox(key: "id"))!
        self.name = try unboxer.unbox(key: "name")
    }
}

struct FUser: User {
    var uid: String
    var email: String
    var fullName: String
    var username: String
    var gender: String
    var DOB: Date
    var profileImageURL: String?
    var phoneNumberNational: String
    var phoneNumberInternational: String
    var verificationCode: String?

    init(username: String, fullName: String, picURL: String){
        self.init()
        self.fullName = fullName
        self.username = username
        self.profileImageURL = picURL
    }
    
    init(userData: FIRUser? = FIRAuth.auth()?.currentUser) {
        uid = (userData?.uid)!
        email = (userData?.email)!
        fullName = (userData?.displayName)!
        profileImageURL = userData?.photoURL?.absoluteString
        username = ""
        gender = ""
        DOB = Date()
        phoneNumberNational = ""
        phoneNumberInternational = ""
        verificationCode = ""
    }
    
    /*
    init(userData: FIRUser?) {
        if let id = userData?.uid {
            uid = id
        }else{
            uid = ""
        }

        if let mail = userData?.providerData.first?.email {
            email = mail
        } else { email = "" }

        if let userFullname = userData?.value(forKey: "fullName") as? String {
            fullName = userFullname
        } else { fullName = "" }

        if let userUsername = userData?.value(forKey: "username") as? String {
            username = userUsername
        } else { username = "" }

        if let userGender = userData?.value(forKey: "gender") as? String {
            gender = userGender
        } else { gender = "" }

        if let userDOB = userData?.value(forKey: "DOB") as? String {
            DOB = try! DateInRegion(string: userDOB, format: .iso8601(options: .withFullDate)).absoluteDate
        } else { DOB = Date() }

        if let userPIURL = userData?.value(forKey: "profileImageURL") as? String {
            profileImageURL = userPIURL
        } else { profileImageURL = "" }

        if let userNN = userData?.value(forKey: "phoneNumberNational") as? String {
            phoneNumberNational = userNN
        } else { phoneNumberNational = "" }

        if let userIN = userData?.value(forKey: "phoneNumberInternational") as? String {
            phoneNumberInternational = userIN
        } else { phoneNumberInternational = "" }

        if let userVC = userData?.value(forKey: "phoneNumberInternational") as? String {
            verificationCode = userVC
        } else { verificationCode = "" }
    }
     
     */
    
    init(uid: String, email: String, fullName: String, username: String, gender: String, profileImageURL: String, phoneNumberNational: String, phoneNumberInternational: String, verificationCode: String) {

        self.uid = uid
        self.email = email
        self.fullName = fullName
        self.username = username
        self.gender   = gender
        self.DOB      = Date()
        self.profileImageURL = profileImageURL

        self.phoneNumberNational      = phoneNumberNational
        self.phoneNumberInternational = phoneNumberInternational
        self.verificationCode = verificationCode
    }


}
