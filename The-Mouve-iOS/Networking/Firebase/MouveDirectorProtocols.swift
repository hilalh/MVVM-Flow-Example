////
////  MouveDirectorProtocols.swift
////  The-Mouve-iOS
////
////  Created by Hilal Habashi on 9/9/16.
////  Copyright © 2016 Hilal Habashi. All rights reserved.
////
//
//import Foundation
//
//typealias MouveDirectorSuccessBlock = (_ mouves: [Mouve]) -> Void
//typealias MouveDirectorFailureBlock = (_ error: NSError?) -> Void
//
//protocol MouveDirectors {
//    var firebaseDirector: FirebaseDirector { get }
////    var googleMapsDirector: GoogleMapsDirector { get }
//}
//
//protocol MouveCacheManager {
//    func save(_ mouves: [Mouve])
//    func unarchived() -> [Mouve]?
//}
//
//protocol MouveDirectorProtocols: MouveDirectors {
//    func allMouves(_ success: MouveDirectorSuccessBlock, failure: MouveDirectorFailureBlock)
//    func saveMouves(_ mouves: [Mouve])
//    func persistedMouves() -> [Mouve]?
//}
//
//protocol MouveNetworkActions {
//    func retrieveMouveInvites()
//    func toggleAttendMouve(_ mouve: FMouve)
//    func deleteMouveInBackground(_ mouve: FMouve)
//    func invitePeopleToMouve(_ mouve: FMouve, people: [FUser])
//    func uploadMouveFiles(_ mouve: FMouve)
////    func createMouveInBackground(creator: String, name: String, address: String, startTime: String, endTime: String, privacy: String, locationLat: String, locationLong: String, backgroundImageURL: String, thumbnailImgURL: String, onCompletion: ((error: NSError?, ref: FIRDatabaseReference) -> ())?)
//}
