////
////  FirebaseDirectorProtocols.swift
////  The-Mouve-iOS
////
////  Created by Hilal Habashi on 9/9/16.
////  Copyright © 2016 Hilal Habashi. All rights reserved.
////
//
//import Foundation
//import Firebase
//
//protocol FirebaseDirectorProtocols {
//    func allMouves(_ completion: @escaping ([FMouve]) -> ())
//    func checkedInMouves(_ completion: ([FMouve]) -> ())
//}
//
//protocol FirebaseManager {
//    func allMouves(_ completion: (FIRDataSnapshot) -> ())
//    func checkedInMouves(_ completion: (FIRDataSnapshot) -> ())
//}
//
//protocol FirebaseBuilder {
//    func mouvesFromResponse(_ response: FIRDataSnapshot) -> [FMouve]
//}
