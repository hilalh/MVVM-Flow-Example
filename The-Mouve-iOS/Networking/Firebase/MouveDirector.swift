////
////  MouveDirector.swift
////  The-Mouve-iOS
////
////  Created by Hilal Habashi on 9/9/16.
////  Copyright © 2016 Hilal Habashi. All rights reserved.
////
//
//import Foundation
//
//final class MouveDirector: MouveDirectorProtocols {
//
//    // MARK: Properties
//
//    var firebaseDirector: FirebaseDirector
////    var googleMapsDirector: GoogleMapsDirector
//    let cacheManager: MouveCacheManager
//
//    // MARK: Init
//
//    init(firebaseDirector: FirebaseDirector, cacheManager: MouveCacheManager) {
//        self.firebaseDirector = firebaseDirector
////        self.googleMapsDirector = googleMapsDirector
//        self.cacheManager = cacheManager
//    }
//
//    // MARK: MouveDirectorProtocols
//
//    func allMouves(_ success: MouveDirectorSuccessBlock, failure: MouveDirectorFailureBlock) {
//        firebaseDirector.allMouves { [unowned self] fMouves in
////            let ids = fMouves.map { $0.googleMouveID }
//
////            self.googleMapsDirector.placesWithIDs(
////                ids,
////                success: { [unowned self] gmPlaces in
////                    let places = self.linkedPlaces(from: fPlaces, and: gmPlaces)
////                    success(places: places)
////                },
////                failure: failure
////            )
//        }
//    }
//
//    func saveMouves(_ mouves: [Mouve]) {
//        cacheManager.save(mouves)
//    }
//
//    func persistedMouves() -> [Mouve]? {
//        return cacheManager.unarchived()
//    }
//
//    // MARK: Helpers
//
//    fileprivate func linkedMouves(from fMouves: [FMouve]) -> [Mouve] {
////        guard fMouves.count == gmPlaces.count else { return [] }
////
////        var places = [Place]()
////        fPlaces.forEach { fPlace in
////            let gmPlace = gmPlaces.filter { $0.placeID == fPlace.googlePlaceID }.first
////            if let gmPlace = gmPlace {
////                let place = Place(firebasePlace: fPlace, googleMapsPlace: gmPlace)
////                places.append(Mouve)
////            }
////        }
//
////        return mouves
//        return [Mouve]()
//    }
//}
