//
//  AppState.swift
//  The-Mouve-iOS
//
//  Created by Hilal Habashi on 11/16/16.
//  Copyright © 2016 Hilal Habashi. All rights reserved.
//

import Foundation
import UIKit
import Crashlytics

class AppState: NSObject {
    
    static let sharedInstance = AppState()
    
    var signedIn = false
    var myUser: abstractUser?
//    var displayName: String?
//    var photoURL: URL?
    
    func logCrashes() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail(myUser?.email)
        Crashlytics.sharedInstance().setUserIdentifier(myUser?.uid)
        Crashlytics.sharedInstance().setUserName(myUser?.username)
    }
}

extension UIDevice {
    class func isSimulator() -> Bool {
        #if IOS_SIMULATOR
            return true
        #else
            return false
        #endif
    }
}
