//: Playground - noun: a place where people can play


import UIKit
import ChameleonFramework
import SnapKit
import XCPlayground
import PlaygroundSupport
import UIKit
import Nuke
let kThemeMainColor = UIColor.flatMint()
class ContactListHeaderView: UIView {
    var headerImageView: UIImageView!
    var profileImageView: UIImageView!
    var subtitleLabel: UILabel!
    var titleLabel: UILabel!
    var leftButtonView: UIImageView?
    var rightButtonView: UIImageView?
    
    
    convenience init(bgColor: UIColor, picURL: URL, title: String, subtitle: String){
        self.init()
        self.backgroundColor = UIColor.white
        
        self.profileImageView = setupProfileImage(image: picURL)
        self.addSubview(self.profileImageView!)
        profileImageView?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top).offset(5)
            make.width.equalTo(self.snp.width).dividedBy(3)
            make.height.equalTo(self.snp.width).dividedBy(3)
            
        }
        self.titleLabel = setupTitle(text: title)
        self.addSubview(self.titleLabel!)
        titleLabel?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.profileImageView.snp.bottom).offset(5)
            make.height.equalTo(25)
            make.width.equalTo(self.snp.width).multipliedBy(0.6)
        }
        self.subtitleLabel = setupSubtitle(text: subtitle)
        self.addSubview(self.subtitleLabel!)
        subtitleLabel?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(5)
            make.height.equalTo(10)
            make.width.equalTo(100)
        }
        self.headerImageView = setupHeaderImageView(color: bgColor)
        self.insertSubview(self.headerImageView!, belowSubview: self.profileImageView!)
        headerImageView?.snp.makeConstraints{(make) -> Void in
            make.centerX.equalTo(self.snp.centerX)
            make.top.equalTo(self.snp.top)
            make.height.equalTo(self.profileImageView.snp.height).dividedBy(2).offset(10)
            make.width.equalTo(self.snp.width)
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.layer.frame.width / 2
    }
    func setupProfileImage(image: URL) -> UIImageView{
        let imageView = UIImageView()
        
        Nuke.loadImage(with: image, into: imageView)
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = imageView.bounds.width / 2
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = kThemeMainColor?.darken(byPercentage: 0.3).cgColor
        imageView.clipsToBounds = true
        
        return imageView
    }
    
    func setupTitle(text: String) -> UILabel{
        let label = UILabel()
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.baselineAdjustment = .alignCenters
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: .headline)

        return label
    }
    
    func setupSubtitle(text: String) -> UILabel{
        let label = UILabel()
        label.textAlignment = .center
        label.adjustsFontForContentSizeCategory = true
        label.adjustsFontSizeToFitWidth = true
        label.baselineAdjustment = .alignCenters
        label.text = text
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.textColor = UIColor.lightGray
        
        return label
    }
    
    
    func setupHeaderImageView(color: UIColor) -> UIImageView{
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = color
        return imageView
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
let picURL = URL(string: "https://scontent-ort2-1.xx.fbcdn.net/v/t1.0-1/p320x320/12439059_1697883227162770_1432690694534869966_n.png?oh=abe134827b60dc9ee6ef8defa521039c&oe=588CD936")
let view = ContactListHeaderView(bgColor: kThemeMainColor!, picURL: picURL!, title: "The Mouve", subtitle: "@themouve")
view.frame = CGRect(x: 0, y: 0, width: 320, height: 180)






//view.startAnimation()




//view.setTimer()
//view.startClockTimer()
//
//
PlaygroundPage.current.liveView = view
PlaygroundPage.current.needsIndefiniteExecution = true
